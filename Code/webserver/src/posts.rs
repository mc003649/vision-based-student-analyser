use actix_web::{ http::header, web, HttpResponse, HttpRequest, Result};
use actix_identity::Identity;
use actix_session::Session;

use futures::StreamExt;
use std::{env, io::Read, path::PathBuf, sync::Arc, fs::File, result};
use image::{load_from_memory};
use uuid::Uuid;
use core::fmt::Display;
use chrono::{NaiveDate, NaiveTime};
use ndarray::prelude::*;

mod db_interface;
mod mime_parser;
mod types;
mod stats;

static SERVER_DIR: &str = "server";

// Post requests for users

pub async fn login(
    database: web::Data<Arc<tokio_postgres::Client>>, 
    session: Session,
    id: Identity,
    params: web::Form<types::Credentials>
) -> Result<HttpResponse>
{

    if let Some(_id) = id.identity(){
        Ok(HttpResponse::Found().header(header::LOCATION, "/pages/index.html").finish())
    }
    else{

        match db_interface::get_user_id_and_password(&***database, &params.email).await{
            Ok((user_id, pass)) => {
                if pass == params.password{
                    id.remember(user_id.to_string().to_owned());
                    session.set("name", "Joe".to_owned())?;
    
                    Ok(HttpResponse::Found().header(header::LOCATION, "/pages/index.html").finish())
                } else {
                    session.set("info", "Incorrect user name and password")?;

                    Ok(HttpResponse::Found().header(header::LOCATION, "/pages/login.html").finish())
                }
                
            }
            Err(message) => {
                session.set("info", message)?;

                Ok(HttpResponse::Found().header(header::LOCATION, "/pages/login.html").finish())
            }
        }
    }
}

pub async fn logout(
    id: Identity,
    session: Session
) -> Result<HttpResponse>
{
    id.forget();
    session.clear();
    Ok(HttpResponse::Found().header(header::LOCATION, "/pages/login.html").finish())
}

pub async fn get_student_table(
    id: Identity,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{

    if let Some(user_id) = id.identity(){
        println!("{:?}", user_id);

        let student_info = db_interface::get_student_table(&***database, user_id.parse::<i32>().unwrap()).await;

        let mut table_rows = Vec::<Vec::<String>>::new();

        for row in 0..student_info.len(){
            table_rows.push(
                vec![student_info[row].0.to_hyphenated().to_string(),
                student_info[row].1.to_owned()]);
        }

        match create_html_table(vec!["Reference", "Email"], table_rows){
            Ok(val) => Ok(HttpResponse::Found().body(val)),
            Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
        }
    } else{
        Err(actix_web::error::ErrorInternalServerError("You must login first"))
    }
}

pub async fn get_students_class(
    database: web::Data<Arc<tokio_postgres::Client>>,
    student_reference: web::Query<types::Responce>,
)-> Result<HttpResponse> {
    let face_ref = student_reference.body.parse::<Uuid>().unwrap_or_else(|e| {
        println!("{:?}", e);
        Uuid::new_v4()
    });

    let class_names = db_interface::get_students_table(&***database, face_ref).await;

    Ok(HttpResponse::Found().body(create_html_select(class_names.1, class_names.0)))
}

pub async fn add_student(
    database: web::Data<Arc<tokio_postgres::Client>>,
    mut body: web::Payload
) -> Result<HttpResponse>{

    let mut payload = web::BytesMut::new();
    while let Some(bytes) = body.next().await {
        payload.extend_from_slice(&bytes?);
    }

    let mut student_data = payload.freeze().to_vec();
    let (email, image) = mime_parser::find_email_and_image(&mut student_data);

    let mut new_uuid: Uuid;
    let mut uuids: Vec<Uuid>;

    loop {
        new_uuid = Uuid::new_v4();
        uuids = db_interface::get_all_uuids(&***database).await;

        if !uuids.contains(&new_uuid){ break; }
    }

    let image = load_from_memory(image).unwrap();
    
    let mut path = PathBuf::new();

    match env::var(SERVER_DIR){
        Ok(val) => path.push(val),
        Err(_e) => path.push("./")
    };

    path.push(format!("..\\students\\{0}.png", new_uuid.to_hyphenated().to_string()));

    if let Err(e) = image.save(path.as_os_str()){
        println!("{:?}",e);
        return Err(actix_web::error::ErrorInternalServerError("Failed to parse image"));
    }

    match db_interface::add_student(&***database, &new_uuid, &email).await {

        Ok(_val) => Ok(HttpResponse::Ok().finish()),
        Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
    }
}

pub async fn enroll_student(
    info: web::Form<types::EnrollInfo>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    db_interface::enroll_student(&***database, &info.face_ref, info.class_id).await;

    Ok(HttpResponse::Ok().finish())
}

pub async fn add_camera_to_class(
    database: web::Data<Arc<tokio_postgres::Client>>,
    camera_connect: web::Form<types::CameraConnect>
) -> Result<HttpResponse>{

    match db_interface::connect_to_camera(
        &***database, 
        &camera_connect.camera_ip, 
        camera_connect.lesson_id)
        .await{
            Ok(_val) => {
                Ok(HttpResponse::Found().header(header::LOCATION, "/pages/teacher/manage_lesson.html").finish())
            },
            Err(_e) => Ok(HttpResponse::Ok().finish())
        }
}

pub async fn get_all_classes(
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse> {
    let class_names = db_interface::get_all_classes(&***database).await;

    Ok(HttpResponse::Found().body(create_html_select(class_names.1, class_names.0)))
}

pub async fn attendace_graph(
    lesson: web::Query<types::SelectLesson>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    let attendance = db_interface::get_attendance(&***database, lesson.lesson_id, lesson.class_id).await;

    let mut path = PathBuf::new();

    match env::var(SERVER_DIR){
        Ok(val) => path.push(val),
        Err(_e) => path.push("./")
    };

    let relative_path = format!("resources/attendance_hist/{0}.png", Uuid::new_v4());
    path.push(relative_path.to_owned());

    let mut data = Array::from_elem(attendance.len(), NaiveTime::from_hms(0, 0, 0));

    for i in 0..attendance.len(){
        data[i] = attendance[i][1].parse::<NaiveTime>().unwrap_or_else(|_|NaiveTime::from_hms(0, 0, 0));
    }

    stats::create_graph(
        data, 
        path.as_path(),
        db_interface::get_lesson_attendace_range(&***database, lesson.lesson_id, lesson.class_id).await
        )
        .unwrap_or_else(|e|
            println!("{:?}", e)
        );

    Ok(HttpResponse::Found().body(relative_path))
}

pub async fn analyse_attendance(
    database: web::Data<Arc<tokio_postgres::Client>>,
    class_id: web::Query<types::GetClass>
)-> Result<HttpResponse>{
    let attendance = db_interface::get_full_attendance(&***database, class_id.class_id)
        .await;

    let mut matrix = Array::from_elem((attendance.len(), attendance[0].len()), 0.0 );

    for lesson in 0..attendance.len(){
        for student in 0..attendance[lesson].len(){
            let val = match matrix.get_mut((lesson, student)){
                Some(val) => val,
                None => unreachable!()
            };

            *val = attendance[lesson][student];
        }
    }

    let pca = stats::pca(matrix, 0.8);

    let mut path = PathBuf::new();

    match env::var(SERVER_DIR){
        Ok(val) => path.push(val),
        Err(_e) => path.push("./")
    };

    let relative_path = format!("resources/scatter/{0}.png", Uuid::new_v4());
    path.push(relative_path.to_owned());

    stats::creat_scatter(pca, path.as_path());

    Ok(HttpResponse::Found().body(relative_path))
}

// Post request for cameras
pub async fn get_face_list(
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    Ok(HttpResponse::Found()
        .body(db_interface::get_all_students(&***database)
            .await
            .to_string()
    ))
}

pub async fn face(
    face_id: web::Path<String>
) -> Result<HttpResponse>{
    let mut path = PathBuf::new(); 

    match env::var(SERVER_DIR){
        Ok(val) => path.push(val),
        Err(_e) => path.push("./")
    };

    path.push(format!("..\\students\\{0}.jpg", face_id.to_string()));

    let mut file = File::open(path.as_path()).unwrap();
    let mut content: Vec<u8> = vec!();

    file.read_to_end(&mut content).unwrap();

    Ok(HttpResponse::Found().body(content))
}

pub async fn get_results(
    database: web::Data<Arc<tokio_postgres::Client>>,
    params: web::Form<types::FaceResults>
) -> Result<HttpResponse>{

    match db_interface::add_to_attendance_log(
        &***database, 
        &params.name, 
        params.similarity, 
        params.lesson_id, 
        &params.camera_mac
    ).await {
        Ok(_val) => Ok(HttpResponse::Ok().finish()),
        Err(e) => {
            println!("{:?}",e);
            Err(actix_web::error::ErrorInternalServerError(e))
        }
    }
}

pub async fn register_camera(
    database: web::Data<Arc<tokio_postgres::Client>>,
    camera_info: web::Form<types::CameraInfo>
) -> Result<HttpResponse>{

    match db_interface::register_camera(
        &***database, 
        &camera_info.camera_ip, 
        &camera_info.camera_mac
    ).await {
        Ok(_val) => Ok(HttpResponse::Ok().finish()),
        Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
    }
}

pub async fn get_cameras_lesson(
    req: HttpRequest,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    let con_info = req.connection_info();
    let host = match con_info.remote(){
        Some(val) => val.split(':').collect::<Vec<&str>>()[0],
        None => ""
    };

    match db_interface::get_cameras_lesson(&***database, &host.to_owned()).await{
        -1 => Err(actix_web::error::ErrorNotFound("")),
        val => Ok(HttpResponse::Found().body(format!("{0}", val)))
    }
}

pub async fn disconnect_camera(
    database: web::Data<Arc<tokio_postgres::Client>>,
    camera_connect: web::Form<types::CameraInfo>
) -> Result<HttpResponse>{

    match db_interface::disconnect_camera(&***database, &camera_connect.camera_mac).await{
        Ok(_val) => Ok(HttpResponse::Ok().finish()),
        Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
    }
}

pub async fn get_camera_table(
    id: Identity,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    if let Some(user_id) = id.identity(){
        println!("{:?}", user_id);

        match create_html_table(
            vec!["Camera ID", "IP address", "Lesson ID"], 
            db_interface::get_all_cameras(&***database).await){
                Ok(val) => Ok(HttpResponse::Found().body(val)),
                Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
            }
    } else {
        Err(actix_web::error::ErrorInternalServerError("You must login first"))
    }
}


pub async fn get_classes(
    id: Identity,
    database: web::Data<Arc<tokio_postgres::Client>>
)-> Result<HttpResponse> {
    if let Some(user_id) = id.identity(){

        let class_names = db_interface::get_users_classes(&***database, user_id.parse::<i32>().unwrap()).await;

        Ok(HttpResponse::Found().body(create_html_select(class_names.1, class_names.0)))
    }else{
        Err(actix_web::error::ErrorInternalServerError("You must login first"))
    }
}

pub async fn create_class(
    class: web::Form<types::NewClass>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    db_interface::create_class(&***database, &class.class_name, &class.email).await;

    Ok(HttpResponse::Ok().finish())
}

pub async fn get_lessons(
    params: web::Query<types::GetClass>,
    database: web::Data<Arc<tokio_postgres::Client>>
)-> Result<HttpResponse> {

    let id_and_date = db_interface::get_lessons(&***database, params.class_id).await;

    Ok(HttpResponse::Found().body(create_html_select(id_and_date.1, id_and_date.0)))
}

pub async fn get_attendance(
    params: web::Query<types::SelectLesson>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    match create_html_table(
        vec!["Student", "Arrival time", "On time?"],
        db_interface::get_attendance(&***database, params.lesson_id, params.class_id).await)
    {
        Ok(val) => Ok(HttpResponse::Found().body(val)),
        Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
    }

}

pub async fn get_teachers(
    id: Identity,
    database: web::Data<Arc<tokio_postgres::Client>>
)-> Result<HttpResponse> {
    if let Some(user_id) = id.identity(){
        if db_interface::is_user_admin(&***database, user_id.parse::<i32>().unwrap()).await{

            match create_html_table(vec!["First name", "Last name", "Email"], 
                db_interface::get_all_teachers(&***database).await){
                Ok(val) => Ok(HttpResponse::Found().body(val)),
                Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
            }
        } else {
            Err(actix_web::error::ErrorInternalServerError("You must be an administrator"))
        }
    } else{
        Err(actix_web::error::ErrorInternalServerError("You must login first"))
    }
} 

pub async fn get_teachers_classes(
    email: web::Form<types::Responce>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    match create_html_table(
        vec![ "Class Id", "Name"], 
        db_interface::get_teachers_classes(&***database, &email.body).await){
            Ok(val) => Ok(HttpResponse::Found().body(val)),
            Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
        }
}

pub async fn get_schedule(
    class: web::Form<types::GetClass>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse>{
    match create_html_table(vec!["Lesson ID", "Class ID", "Date", "Lesson start", "Lesson end", "Attendance start", "Attendance end"], 
        db_interface::get_schedule(&***database, class.class_id).await){
            Ok(val) => Ok(HttpResponse::Found().body(val)),
            Err(e) => Err(actix_web::error::ErrorInternalServerError(e))
    }
}

pub async fn update_lesson(
    params: web::Form<types::LessonInfo>,
    database: web::Data<Arc<tokio_postgres::Client>>
) -> Result<HttpResponse> {

    db_interface::update_lesson(&***database, 
        params.class_id, 
        params.lesson_id, 
        NaiveDate::parse_from_str(params.l_date.as_str(), "%Y-%m-%d").unwrap(), 
        NaiveTime::parse_from_str(params.l_start.as_str(), "%H:%M").unwrap(),
        NaiveTime::parse_from_str(params.l_end.as_str(), "%H:%M").unwrap(),
        NaiveTime::parse_from_str(params.a_start.as_str(), "%H:%M").unwrap(),
        NaiveTime::parse_from_str(params.a_end.as_str(), "%H:%M").unwrap()).await;

    Ok(HttpResponse::Ok().finish())
}

fn create_html_select<I: Display, V: Display>(
    items: Vec<I>,
    values: Vec<V>
) -> String{
    let mut out = String::new();

    for i in 0..items.len(){
        out.push_str(format!("<option value=\"{0}\">{1}</option>", values[i], items[i]).as_str())
    }

    out
}

fn create_html_table<H : Display, R: Display>(
    headers: Vec<H>, 
    rows: Vec<Vec<R>>
) -> result::Result<String, String>{
    let mut table = "<tr>".to_owned();

    if rows.len() == 0{
        table.push_str("</tr>");
        return Ok(table);
    }

    if headers.len() != rows[0].len(){
        return result::Result::Err("Number of headers and columns does not match".to_owned());
    } 

    for i in 0..headers.len(){
        table.push_str(format!("<th>{0}</th>",headers[i]).as_str());
    }

    table.push_str("</tr>");

    for row in 0..rows.len(){
        table.push_str("<tr>");

        for col in 0..rows[row].len(){
            table.push_str(format!("<td>{0}</td>",rows[row][col]).as_str());
        }
        
        table.push_str("</tr>");
    }

    Ok(table)
}
