extern crate actix_web;

use std::{env, io, sync::Arc};

use actix_files as fs;
use actix_session::CookieSession;
use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::http::{header, StatusCode};
use actix_web::{
    guard, middleware, web, App, HttpRequest, HttpResponse, HttpServer,
    Result,
};

use tokio_postgres::NoTls;
use tokio;

mod posts;

// 404 handler
async fn p404() -> Result<fs::NamedFile> {
    Ok(fs::NamedFile::open("../pages/404.html")?.set_status_code(StatusCode::NOT_FOUND))
}

async fn redirect(req: HttpRequest) -> HttpResponse{
    println!("{:?}", req);
    HttpResponse::Found()
        .header(header::LOCATION, "/pages/login.html")
        .finish()
}

#[actix_rt::main]
async fn main() -> io::Result<()> {
let domain = "192.168.1.29";

    //Start database

    let (client, connection) = match tokio_postgres::connect("host=localhost user=root port=5432 password=vbaa1 dbname=vbaa_db", NoTls).await{
        Ok((cl, co)) => (cl, co),
        Err(e) => {
            panic!(io::Error::new(io::ErrorKind::Interrupted, e))
        }
    };
        
    // The connection object performs the actual communication with the database,
    // so spawn it off to run on its own.
    tokio::spawn(async move {
        if let Err(e) = connection.await {
            eprintln!("connection error: {}", e);
        }
    });

    let arc_client = Arc::new(client);

    //Start server
    env::set_var("RUST_LOG", "actix_web=debug");
    env_logger::init();

    HttpServer::new(move || {
        App::new()
            .data(arc_client.clone())
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&[0; 32])
                    .domain(domain)
                    .name("user")
                    .max_age(86400) //A day
                    .secure(false)))
            // cookie session middleware only for client javascript
            .wrap(CookieSession::signed(&[0; 32])
                .domain(domain)
                .name("public")
                .secure(false)
                .http_only(false))
            // enable logger - always register actix-web Logger middleware last
            .wrap(middleware::Logger::default())
            .data(web::JsonConfig::default().limit(4098))
            // static files
            .service(fs::Files::new("/pages", "pages").show_files_listing())
            // redirect
            .service(web::resource("/").route(web::get().to(redirect)))
            // get the list of all students
            .service(web::resource("/students_info").route(web::get().to(posts::get_student_table)))
            // add student to database
            .service(web::resource("/add_student").route(web::post().to(posts::add_student)))
            // add student to database
            .service(web::resource("/enroll_student").route(web::post().to(posts::enroll_student)))
            // get list of students
            .service(web::resource("/get_face_list").route(web::get().to(posts::get_face_list)))
            // get face
            .service(web::resource("/face/{id}").route(web::get().to(posts::face)))
            // get face results
            .service(web::resource("/face_result").route(web::post().to(posts::get_results)))
            // user login 
            .service(web::resource("/login").route(web::post().to(posts::login)))
            // user logout
            .service(web::resource("/logout").route(web::post().to(posts::logout)))
            // lets a user add a camera to their lesson
            .service(web::resource("/add_camera_to_class").route(web::post().to(posts::add_camera_to_class)))
            // allows cameras to register themselfs to this server
            .service(web::resource("/register_camera").route(web::post().to(posts::register_camera)))
            // allows the camera to retirieve the lesson it was assigned
            .service(web::resource("/get_cameras_lesson").route(web::get().to(posts::get_cameras_lesson)))
            // disconnect camera from this server
            .service(web::resource("/disconnect_camera").route(web::post().to(posts::disconnect_camera)))
            // get the list of all cameras
            .service(web::resource("/camera_info").route(web::get().to(posts::get_camera_table)))
            // get the classes
            .service(web::resource("/get_classes").route(web::get().to(posts::get_classes)))
            // get all of the classes
            .service(web::resource("/get_all_classes").route(web::get().to(posts::get_all_classes)))
            // get all classes student isn't taking
            .service(web::resource("/get_students_classes").route(web::get().to(posts::get_students_class)))
            // get create new class
            .service(web::resource("/create_class").route(web::post().to(posts::create_class)))
            // get the lessons for a class
            .service(web::resource("/get_lessons").route(web::get().to(posts::get_lessons)))
            // get the attendance
            .service(web::resource("/attendance").route(web::get().to(posts::get_attendance)))
            // get all the teachers class
            .service(web::resource("/get_teachers_classes").route(web::post().to(posts::get_teachers_classes)))
            // get all the teachers
            .service(web::resource("/get_teachers").route(web::get().to(posts::get_teachers)))
            // get all the classes lessons
            .service(web::resource("/get_schedule").route(web::post().to(posts::get_schedule)))
            // get all the classes lessons
            .service(web::resource("/update_lesson").route(web::post().to(posts::update_lesson)))

            // get attendance graph
            .service(web::resource("/attendace_graph").route(web::get().to(posts::attendace_graph)))
            // get attendance graph
            .service(web::resource("/analyse_attendance").route(web::get().to(posts::analyse_attendance)))
            // default
            .default_service(
                // 404 for GET request
                web::resource("")
                    .route(web::get().to(p404))
                    // all requests that are not `GET`
                    .route(
                        web::route()
                            .guard(guard::Not(guard::Get()))
                            .to(HttpResponse::MethodNotAllowed),
                    ),
            )
    })
    .bind(format!("{0}:8088", domain))?
    .run()
    .await
}


