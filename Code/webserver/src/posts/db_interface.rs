use tokio_postgres::{Row, Error};
use std::net::IpAddr;
use eui48::MacAddress;
use uuid::Uuid;
use chrono::{NaiveTime, NaiveDate, Datelike, Timelike};

// Common interface
pub async fn get_user_id_and_password(
    database: &tokio_postgres::Client,
    email: &String 
) -> Result<(i32, String), String>{
    let rows = database.query("SELECT hash, user_id 
        FROM vbaa_users 
        WHERE email = $1;", &[&email])
            .await
            .unwrap_or_else(|e|{
                println!("{:?}", e); 
                vec![]
            });
            
    if rows.len() == 1
    {
        let hash_pass: &str = rows[0].get(0);
        let user_id: i32 = rows[0].get(1);

        return Ok((user_id, hash_pass.to_owned()));
    }

    Err("No user found".to_owned())
}

pub async fn get_student_table(
    database: &tokio_postgres::Client,
    user_id: i32
)-> Vec<(Uuid, String)>{
    let mut out = Vec::new();

    if is_user_admin(database, user_id).await{
        let rows = database.query("SELECT student.face_reference, student.email
            FROM student", &[])
            .await
            .unwrap_or_else(|e|{
                println!("{:?}", e); 
                vec![]
        });
    
        for row in rows{
            let email: Uuid = row.get(0);
            let reference: String = row.get(1);
           // let class_name: String = row.get(2);

            out.push((email, reference));
        }

    }
    
    out
}

pub async fn get_all_classes(
    database: &tokio_postgres::Client
) -> (Vec<String>, Vec<String>){
    let mut out = (Vec::new(), Vec::new());

    let rows = database.query("SELECT class_id, class_name FROM cid", &[])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );

    for row in rows{
        let class_id: i32 = row.get(0);
        let class_name: String = row.get(1);

        out.0.push(class_id.to_string());
        out.1.push(class_name);
    }

    out
}

pub async fn get_students_table(
    database: &tokio_postgres::Client,
    face_ref: Uuid
) -> (Vec<String>, Vec<String>){
    let mut out = (Vec::new(), Vec::new());

    let rows = database.query("
    SELECT cid.class_id, cid.class_name
	    FROM cid
	    WHERE cid.class_id NOT IN (
		    SELECT cid.class_id
        	    FROM student
        	    INNER JOIN vbaa_group ON student.student_id = vbaa_group.student_id 
				    AND student.face_reference = $1
			    INNER JOIN cid ON vbaa_group.class_id = cid.class_id
	    )", &[&face_ref])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
    });

    for row in rows{
        let class_id: i32 = row.get(0);
        let class_name: String = row.get(1);

        out.0.push(class_id.to_string());
        out.1.push(class_name);
    }

    out
}

pub async fn get_attendance(
    database: &tokio_postgres::Client,
    lesson_id: i32,
    class_id: i32
) -> Vec<Vec<String>>{
    let mut out = Vec::new();

    let rows = database.query("
    SELECT 
        student.email, 
        attendance_log.time_entered, 
        CASE WHEN attendance_log.time_entered < lesson.attendance_end THEN 'TRUE' else 'FALSE' END AS on_time
    FROM lesson
    INNER JOIN attendance_log ON attendance_log.lesson_id = lesson.lesson_id
    INNER JOIN student ON student.student_id = attendance_log.student_id
    WHERE lesson.lesson_id = $1 AND lesson.class_id = $2", &[&lesson_id, &class_id])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
    });

    for row in rows{
        let email: String = row.get(0);
        let time: NaiveTime = row.get(1);
        let on_time: String = row.get(2);

        out.push(vec![email, 
            time.to_string(), 
            on_time]);
    }

    out
}

pub async fn get_full_attendance(
    database: &tokio_postgres::Client,
    class_id: i32
) -> Vec<Vec<f64>>{
    //The output should contain a vector of lessons where a lesson contains the time of arrival for each student
    let mut out = Vec::new();

    let rows = database.query("
    SELECT 
        vbaa_group.student_id,
	    attendance_log.lesson_id, 
        attendance_log.time_entered, 
        lesson.attendance_start,
        lesson.lesson_end
	FROM attendance_log
        INNER JOIN lesson ON lesson.lesson_id = attendance_log.lesson_id
		RIGHT OUTER JOIN vbaa_group ON vbaa_group.student_id = attendance_log.student_id AND vbaa_group.class_id = lesson.class_id
		WHERE vbaa_group.class_id = $1
		ORDER BY 
			attendance_log.student_id ASC, 
			attendance_log.lesson_id ASC", &[&class_id])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );

    let mut student_ids = Vec::new();

    // Find all the students that are meant to be in this class
    for i in 0..rows.len(){
        let id: i32 = rows[i].get(0);
        if !student_ids.contains(&id){
            student_ids.push(id);
        }
    }

    let mut lessons_processed = Vec::new();
    for i in 0..rows.len(){
        let lesson_id:i32 = match rows[i].try_get(1){
            Ok(val) => val,
            _ => break
        };

        if !lessons_processed.contains(&lesson_id){
            let att_start: NaiveTime = rows[i].get(3);
            let lesson_end: NaiveTime = rows[i].get(4);

            let mut lesson = vec![(lesson_end-att_start).num_seconds() as f64; student_ids.len()];

            for j in i..rows.len(){

                let cur_lesson: i32 = match rows[j].try_get(1){
                    Ok(val) => val,
                    _ => break
                };

                if lesson_id == cur_lesson{
                    let time_entered: NaiveTime = rows[j].get(2);
                    let student_index = match student_ids.binary_search(&rows[j].get(0)){
                        Ok(val) => val,
                        _ => break
                    };

                    lesson[student_index] = (time_entered - att_start).num_seconds() as f64;
                } 
            }

            lessons_processed.push(lesson_id);
            out.push(lesson);
        }
    }

    println!("{:?}", out);
    out.to_owned()
}

pub async fn get_lesson_attendace_range(
    database: &tokio_postgres::Client,
    lesson_id: i32,
    class_id: i32
)->(NaiveTime, NaiveTime){
    let mut out = (NaiveTime::from_hms(23, 59, 59), //attendance start
        NaiveTime::from_hms(0, 0, 0)); //attendance end

    let rows = database.query("SELECT lesson.attendance_start, lesson.attendance_end
        FROM lesson 
        WHERE lesson_id = $1 AND class_id = $2", &[&lesson_id, &class_id])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );

    out.0 = rows[0].get(0);
    out.1 = rows[0].get(1);

    out
}


// Camera interface
pub async fn register_camera(
    database: &tokio_postgres::Client,
    ip: &String,
    mac: &String
) -> Result<Vec<Row>, Error> {

    let adr_ip: IpAddr = ip.parse().unwrap();
    let adr_mac: eui48::MacAddress = mac.parse().unwrap();

    database.query("INSERT INTO camera (ip_address, mac_address) 
        VALUES ($1, $2)", &[&adr_ip, &adr_mac])
        .await
}

pub async fn get_cameras_lesson(
    database: &tokio_postgres::Client,
    ip: &String
) -> i32{
    let adr_ip: IpAddr = ip.parse().unwrap();

    let rows = database.query("SELECT lesson_id FROM camera WHERE ip_address = $1", &[&adr_ip])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        });

    if rows.len() != 0{
        let lesson: i32 = rows[0].get(0);
        lesson
    } else{
        -1
    }
}

pub async fn disconnect_camera(
    database: &tokio_postgres::Client,
    mac: &String
) -> Result<Vec<Row>, Error>{
    database.query("UPDATE camera
        SET time_disconnected = CURRENT_TIMESTAMP
        WHERE mac = $1", &[mac])
        .await
}

pub async fn get_all_cameras(
    database: &tokio_postgres::Client
) -> Vec<Vec<String>>{
    let mut out = Vec::new();

    let rows = database.query("SELECT camera_id, ip_address, lesson_id FROM camera", &[])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        });

    for row in rows{
        let camera_id: i32 = row.get(0);
        let ip_address: IpAddr = row.get(1);
        let lesson_id: String = match row.try_get::<usize, i32>(2){
            Ok(val) => val.to_string(),
            _ => "None".to_owned()
        };

        out.push(vec![
            camera_id.to_string(), 
            ip_address.to_string(), 
            lesson_id.to_owned()]);
    }

    out
}

pub async fn add_to_attendance_log(
    database: &tokio_postgres::Client,
    id: &String,
    similarity: f32,
    lesson: i32,
    mac: &String
) -> Result<Vec<Row>, Error>{
    let adr_mac: MacAddress = mac.parse().unwrap();
    let id_name: Uuid = id.parse().unwrap();
    let percent_conf: i32 = (similarity * 100.0).round() as i32;

    database.query("INSERT INTO attendance_log (lesson_id, student_id, camera_id, time_entered, confidance) 
        SELECT $3, student.student_id, camera.camera_id, CURRENT_TIMESTAMP, $2 
        FROM student 
        INNER JOIN camera ON camera.mac_address = $4
        WHERE student.face_reference = $1 ", &[&id_name, &percent_conf, &lesson, &adr_mac])
        .await
}

pub async fn get_all_students(
    database: &tokio_postgres::Client
) -> String{
    let mut out = String::new();

    let rows = database.query("SELECT face_reference FROM student", &[])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        });
    
        for row in rows{
        let id: Uuid = row.get(0);
        out.push_str(&format!("{0}\n", id.to_string()).to_string());
    }

    out
}

// Teacher interface
pub async fn connect_to_camera(
    database: &tokio_postgres::Client,
    ip: &String,
    lesson: i32
) -> Result<Vec<Row>, Error> {
    let adr_ip: IpAddr = ip.parse().unwrap();

    database.query("UPDATE camera 
        SET time_connected = CURRENT_TIMESTAMP, lesson_id = $2
        WHERE ip_address = $1", &[&adr_ip, &lesson])
        .await
}

pub async fn get_users_classes(
    database: &tokio_postgres::Client,
    user_id: i32
) -> (Vec<i32>, Vec<String>){
    let mut out = (Vec::new(), Vec::new());

    let rows = database.query("
        SELECT cid.class_id, cid.class_name 
        FROM cid
        INNER JOIN class ON class.class_id = cid.class_id AND class.user_id = $1", &[&user_id])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        });

    for row in rows{
        let id: i32 = row.get(0);
        let name: String = row.get(1);

        out.0.push(id);
        out.1.push(name);
    }

    out
}

pub async fn get_lessons(
    database: &tokio_postgres::Client,
    class_id: i32
) -> (Vec<i32>, Vec<NaiveDate>){
    let mut out = (Vec::new(), Vec::new());

    let rows = database.query("SELECT lesson_id, date FROM lesson WHERE class_id = $1", &[&class_id])
    .await
    .unwrap_or_else(|e|{
        println!("{:?}", e); 
        vec![]
    });

    for row in rows{
        let lesson: i32 = row.get(0);
        let date: NaiveDate = row.get(1);

        out.0.push(lesson);
        out.1.push(date);
    }

    out
}

// Management interface
pub async fn is_user_admin(
    database: &tokio_postgres::Client,
    user_id: i32
) -> bool{
    let rows = database.query("
SELECT 
	CASE WHEN vbaa_users.user_type = 'admin' THEN true
	ELSE false END AS is_admin
FROM vbaa_users
	WHERE vbaa_users.user_id = $1", &[&user_id])
    .await
    .unwrap_or_else(|e|{
        println!("{:?}", e); 
        vec![]
    });

    let out: bool = rows[0].get(0);

    out
}

pub async fn add_student(
    database: &tokio_postgres::Client,
    face_reference_id: &uuid::Uuid,
    email: &String
) -> Result<Vec<Row>, Error>{
    database.query("INSERT INTO student (face_reference, email) 
        VALUES ($1, $2)", &[face_reference_id, email])
        .await
}

pub async fn get_all_uuids(
    database: &tokio_postgres::Client
) -> Vec<Uuid>{
    let mut out = Vec::new();

    let rows = database.query("SELECT face_reference FROM student", &[])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        });

    for row in rows{
        let temp: Uuid = row.get(0);
        out.push(temp.to_owned());
    }

    out
}

pub async fn get_all_teachers(
    database: &tokio_postgres::Client
) -> Vec<Vec<String>>{
    let mut out = Vec::<Vec<String>>::new();

    let rows = database.query("SELECT first_name, last_name, email FROM vbaa_users WHERE user_type = 'teacher'", &[])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
    });

    for row in rows{
        out.push(vec![row.get(0), row.get(1), row.get(2)]);
    }

    out
}

pub async fn get_teachers_classes(
    database: &tokio_postgres::Client,
    email: &String,
) -> Vec<Vec<String>>{
    let mut out = Vec::<Vec<String>>::new();

    let rows = database.query("SELECT cid.class_id, cid.class_name 
        FROM cid
        INNER JOIN class ON class.class_id = cid.class_id 
		INNER JOIN vbaa_users ON class.user_id = vbaa_users.user_id AND vbaa_users.email = $1", &[email])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
    });

    for row in rows{
        let class_id: i32 = row.get(0);
        out.push(vec![class_id.to_string(), row.get(1)]);
    }

    out
}

pub async fn get_schedule(
    database: &tokio_postgres::Client,
    class: i32
) -> Vec<Vec<String>>{
    let mut out = Vec::<Vec<String>>::new();

    let rows = database.query("SELECT lesson_id, date, lesson_start, lesson_end, attendance_start, attendance_end 
        FROM lesson
        WHERE class_id = $1", &[&class])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );

    for row in rows{
        let lesson: i32 = row.get(0);
        let date: NaiveDate = row.get(1);
        let l_start: NaiveTime = row.get(2);
        let l_end: NaiveTime = row.get(3);
        let a_start: NaiveTime = row.get(4);
        let a_end: NaiveTime = row.get(5);

        out.push(vec![
            lesson.to_string(), 
            class.to_string(),
            format!("{0}/{1}/{2}", date.day(), date.month(), date.year()), 
            format!("{0}:{1}", l_start.hour(), l_start.minute()),
            format!("{0}:{1}", l_end.hour(), l_end.minute()),
            format!("{0}:{1}", a_start.hour(), a_start.minute()),
            format!("{0}:{1}", a_end.hour(), a_end.minute()),
            ])
    }

    out
}

pub async fn create_class(
    database: &tokio_postgres::Client,
    class_name: &String,
    email: &String
){
    database.query("INSERT INTO cid (class_name) VALUES ($1)", &[class_name])
        .await        
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );

    database.query("INSERT INTO class (class_id, user_id) 
        SELECT cid.class_id, vbaa_users.user_id
            FROM cid
            INNER JOIN vbaa_users ON vbaa_users.email = $2
            WHERE cid ON cid.class_name = $1", &[class_name, email])
        .await        
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );

}

pub async fn update_lesson(
    database: &tokio_postgres::Client,
    class_id: i32,
    lesson_id: i32,
    date: NaiveDate,
    l_start: NaiveTime,
    l_end: NaiveTime,
    a_start: NaiveTime,
    a_end: NaiveTime,
){
    println!("{:?}", lesson_id);
    if lesson_id == 0{
        database.query("INSERT INTO lesson (class_id, date, lesson_start, lesson_end, attendance_start, attendance_end) 
        VALUES ( $1, $2, $3, $4, $5, $6)", 
            &[&class_id, &date, &l_start, &l_end, &a_start, &a_end]).await        
                .unwrap_or_else(|e|{
                    println!("{:?}", e); 
                    vec![]
            });
    } else{
        database.query("UPDATE lesson SET date=$3, lesson_start=$4, lesson_end=$5, attendance_start=$6, attendance_end=$7 
            WHERE class_id=$1 AND lesson_id=$2", 
            &[&class_id, &lesson_id, &date, &l_start, &l_end, &a_start, &a_end]).await        
                .unwrap_or_else(|e|{
                    println!("{:?}", e); 
                    vec![]
            });
    }
}


pub async fn enroll_student(
    database: &tokio_postgres::Client,
    face_ref: &String,
    class_id: i32
){
    let face: Uuid = face_ref.parse().unwrap();
    database.query("INSERT INTO vbaa_group (class_id, student_id)
        SELECT $1, student.student_id
            FROM student
                WHERE student.face_reference = $2", &[&class_id, &face])
        .await
        .unwrap_or_else(|e|{
            println!("{:?}", e); 
            vec![]
        }
    );
}