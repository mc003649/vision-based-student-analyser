enum State{
    BEGIN,
    LABEL,
    LABEL_POST,
    NAME,
    NAME_POST,
    TYPE,
    EMAIL,
    IMAGE
}

pub fn find_email_and_image( data: &mut Vec<u8> ) -> (String, &[u8]){
    let mut email = "".to_owned();
    let mut post_fix = 0;
    let mut image_slice: &[u8] = &[0];

    let mut token = data.iter();

    let mut found_email = false;
    let mut found_image = false;

    let mut state = State::BEGIN;

    loop{
        state = match state {
            State::BEGIN => {
                match token.next(){
                    None=>break,
                    Some(byte) if b'-' == *byte => State::LABEL,
                    _ => State::BEGIN
                }
            }
            State::LABEL => {
                match token.as_slice().split(|byte| *byte == b'\r').next(){
                    None => break,
                    Some(r) => post_fix = r.len() + 5,
                };
                State::LABEL_POST
            }
            State::LABEL_POST =>{
                match token.next(){
                    None=>break,
                    Some(byte) if b'n' == *byte => State::NAME,
                    _ => State::LABEL_POST
                }
            }
            State::NAME => {
                match consume(&mut token, b"ame"){// the 'n' in 'name' is already consumed
                    true => State::NAME_POST,
                    false => State::LABEL_POST
                }
            }
            State::NAME_POST => {
                match token.next(){
                    None=>break,
                    Some(byte) if b'\"' == *byte => State::TYPE,
                    _ => State::NAME_POST
                }
            }
            State::TYPE => {
                match consume_into_string_until(&mut token, b'\"').as_str(){
                    "email" => State::EMAIL,
                    "image_file" => State::IMAGE,
                    _ => State::BEGIN
                }
            }
            State::EMAIL => {
                match token.next(){
                    None=>break,
                    Some(byte) if byte.is_ascii_alphabetic() =>{
                        email.push(*byte as char);
                        email.push_str(consume_into_string_until(&mut token, b'\r').as_str());
                        found_email = true;
                        State::BEGIN
                    },
                    _ => State::EMAIL
                }
            }
            State::IMAGE => {
                let temp_slice = token.as_slice();

                match token.next(){
                    None=>break,
                    Some(byte) if byte == &b'\xff' => { 
                        image_slice = temp_slice.split_at(temp_slice.len()-post_fix).0;
                        found_image = true;
                        State::BEGIN
                    },
                    _ => State::IMAGE
                }
            }
        };

        if found_email && found_image {
            break;
        }
    }

    return (email, image_slice);
}

fn consume(
    token: &mut std::slice::Iter<'_, u8>, 
    to_consume: &[u8]
) -> bool{
    let mut consumed = true;
    let mut clone = token.clone();

    for i in to_consume.iter(){
        match clone.next(){
            None => consumed = false,
            Some(byte) if (byte != i) => consumed = false,
            _ => continue
        }
    }

    if consumed{
        *token = clone;
    }

    consumed
}

fn consume_into_string_until(
    token: &mut std::slice::Iter<'_, u8>, 
    until: u8
) -> String{
    let mut out = String::new();

    loop{
        match token.next(){
            None=>break,
            Some(byte) if byte.is_ascii() => {
                if until != *byte{
                    out.push(*byte as char)
                } else{
                    break;
                }
            }
            _ => continue
        }
    }
    out
}