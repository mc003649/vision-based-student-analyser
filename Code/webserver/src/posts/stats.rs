use ndarray::prelude::*;
use chrono::{NaiveTime};
use plotters::prelude::*;
use ndarray_stats::CorrelationExt;
use rulinalg::matrix::{Matrix, BaseMatrix};
use rulinalg::norm::Euclidean;
use std::f64;

pub fn create_graph(
    data: Array1<NaiveTime>, 
    graph_name: &std::path::Path,
    attendance_range: (NaiveTime, NaiveTime)
) -> Result<(), Box<dyn std::error::Error>>{

    let root = BitMapBackend::new(graph_name, (1240, 768)).into_drawing_area();
    let att_rang = attendance_range.1 - attendance_range.0;

    let mut min = NaiveTime::from_hms(23, 59, 59);
    let mut max = NaiveTime::from_hms(0, 0, 0);

    for &time in data.iter(){
        if min > time{
            min = time;
        }

        if max < time{
            max = time;
        }
    }

    if min < attendance_range.0{
        // someone entered very early

    } else{
        min = attendance_range.0;
    }

    if max > attendance_range.1{
        // someone was late
        
    } else{
        max = attendance_range.1;
    }

    root.fill(&WHITE)?;
    let full_range = max - min;

    let mut chart = ChartBuilder::on(&root)
        .x_label_area_size(35)
        .y_label_area_size(40)
        .margin(5)
        .caption("Attendance Histogram", ("sans-serif", 40.0).into_font())
        .build_ranged(0..(full_range.num_minutes()+2), 0u32..10u32)?;

    chart
        .configure_mesh()
        .disable_x_mesh()
        .line_style_1(&WHITE.mix(0.3))
        .x_label_offset(30)
        .y_desc("Number of students entered")
        .x_desc("Minutes from the start of attendance")
        .axis_desc_style(("sans-serif", 15).into_font())
        .draw()?;
    
    chart.draw_series(
        Histogram::vertical(&chart)
            .style(GREEN.mix(0.5).filled())
            .data(data.iter().map(|x|{
                let temp_x = *x - min;
                if temp_x < att_rang{
                    (temp_x.num_minutes(), 1)
                } else {
                    (-1,0)
                }
            })
        )
    )?;

    chart.draw_series(
        Histogram::vertical(&chart)
            .style(RED.mix(0.5).filled())
            .data(data.iter().map(|x|{
                let temp_x = *x - min;
                if temp_x < att_rang{
                    (-1,0)
                } else {
                    (temp_x.num_minutes(), 1)
                }
            })
        )
    )?;

    Ok(())
}

pub fn creat_scatter(
    data: Array2<f64>,
    graph_name: &std::path::Path,
)-> Result<(), Box<dyn std::error::Error>>{
    let root = BitMapBackend::new(graph_name, (1240, 768)).into_drawing_area();
    root.fill(&WHITE)?;

    if data.shape()[0] != 2{
        return Ok(());
    }
    let (mut min_x, mut max_x, mut min_y, mut max_y) = (f64::INFINITY, f64::NEG_INFINITY, f64::INFINITY, f64::NEG_INFINITY);

    for item in data.gencolumns(){

        if item[0] > max_x{
            max_x = item[0];
        } 
        if item[0] < min_x{
            min_x = item[0];
        }

        if item[1] > max_y{
            max_y = item[1];
        }
        if item[1] < min_y{
            min_y = item[1];
        }
    }

    let (mean_x, mean_y) = ((max_x - min_x)/2.0, (max_y - min_y)/2.0);

    let mut chart = ChartBuilder::on(&root)
        .x_label_area_size(35)
        .y_label_area_size(40)
        .margin(5)
        .caption("Attendance Scatter", ("sans-serif", 40.0).into_font())
        .build_ranged((min_x-mean_x*0.05)..(max_x+mean_x*0.05), (min_y-mean_y*0.05)..(max_y+mean_y*0.05))?;

    chart
        .configure_mesh()
        .disable_x_mesh()
        .disable_y_mesh()
        .line_style_1(&WHITE.mix(0.3))
        .x_label_offset(30)
        .y_desc("PC1")
        .x_desc("PC0")
        .axis_desc_style(("sans-serif", 15).into_font())
        .draw()?;

    chart.draw_series(
        data
            .gencolumns()
            .into_iter()
            .map(|item| Circle::new((item[0], item[1]), 4, GREEN.filled())),
        )?;

    Ok(())
}

pub fn pca(
    data: Array2<f64>,
    percent_to_keep: f32
) -> Array2<f64>{
    let original_shape = data.shape();

    let col_mean = data.mean_axis(Axis(0)).unwrap();
    let centered = data.to_owned() - col_mean;

    let m_copy = Matrix::new(original_shape[1], original_shape[0], data.as_slice().unwrap());

    let data_cov = match centered.cov(0.0){
        Ok(val) => val,
        _ => unreachable!()
    };

    let shape = data_cov.shape();
    let temp = match data_cov.as_slice(){
        Some(val) => val,
        _ => unreachable!()
    };

    let matrix = Matrix::new(shape[0], shape[1], temp); 
    println!("{:?}", matrix);
    let  (eigenvalues, eignevectors) = match matrix.eigendecomp(){
        Ok(val) => val,
        _ => unreachable!()
    };

    println!("{:?}, {:?}", eigenvalues, eignevectors);

    let unit_eignevectors = &eignevectors / &eignevectors.norm(Euclidean);

    let transformed = m_copy * unit_eignevectors;

    println!("{:?}", transformed);

    let mut out = Array::from_elem((2, original_shape[1]), 0.0 );

    for (tran_col, mut out_col) in transformed.col_iter().zip(out.axis_iter_mut(Axis(0))){
        println!("{:?}", out_col);
        println!("{:?}", tran_col);
        for (tran_item, out_item) in tran_col.iter().zip(out_col.iter_mut()){
            *out_item = *tran_item;
        }
    }

    out
}