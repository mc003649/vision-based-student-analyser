use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct StudentCred {
    email: String,
    #[serde(with = "serde_bytes")]
    file: Vec<u8>,
}

#[derive(Serialize, Deserialize)]
pub struct Credentials {
    pub email: String,
    pub password: String,
}

#[derive(Serialize, Deserialize)]
pub struct Responce{
    pub body: String,
}

#[derive(Serialize, Deserialize)]
pub struct FaceResults{
    pub name: String,
    pub similarity: f32,
    pub lesson_id: i32,
    pub camera_mac: String
}

#[derive(Serialize, Deserialize)]
pub struct CameraInfo{
    pub camera_ip: String,
    pub camera_mac: String
}

#[derive(Serialize, Deserialize)]
pub struct CameraConnect{
    pub camera_ip: String,
    pub lesson_id: i32
}

#[derive(Serialize, Deserialize)]
pub struct GetClass{
    pub class_id: i32,
}

#[derive(Serialize, Deserialize)]
pub struct SelectLesson{
    pub class_id: i32,
    pub lesson_id: i32
}

#[derive(Serialize, Deserialize)]
pub struct LessonInfo{
    pub class_id: i32,
    pub lesson_id: i32,
    pub l_date: String,
    pub l_start: String,
    pub l_end: String,
    pub a_start: String,
    pub a_end: String,
}

#[derive(Serialize, Deserialize)]
pub struct NewClass{
    pub email: String,
    pub class_name: String,
}

#[derive(Serialize, Deserialize)]
pub struct EnrollInfo{
    pub face_ref: String,
    pub class_id: i32,
}