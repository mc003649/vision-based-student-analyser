#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <fstream>
#include <iostream>

#include <dirent.h>
#include <unistd.h>

#include <visageSDK/visageVision.h>
#include <visageSDK/vs_core_core_c.h>
#include <visageSDK/VisageFaceRecognition.h>

#include <raspicam/raspicam.h>

#include <curl/curl.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/core/types_c.h>



static std::string licenseKey = "../license/636-325-040-132-080-094-863-427-607-763-824.vlc";
