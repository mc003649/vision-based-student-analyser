#include <main.hpp>

#define MAX_PARALLEL 10  
#define VISAGE_MAX_FACES_TO_HANDLE 5
#define DATAPATH "../data"
#define DATAPATH2 "../data/bdtsdata/NN/fr.bin"
#define FACES "./faces/example.jpg"
#define SERVER "192.168.0.50:8088"

using namespace std;
using namespace VisageSDK;

namespace VisageSDK //all visage|SDK calls are in visageSDK namespace
{
    void initializeLicenseManager(const char *licenseKeyFileFolder);
}

struct MemoryStruct {
  unsigned char *memory;
  size_t size;
};

/**
 * Visage analysis curenty estimates this seven emotions
 */
const char *visage_emotions[7] = {"Emotion", "Anger", "Disgust", "Fear", "Happiness", "Sadness", "Surprise"};

/**
 * Prints ALL defined (existing) feature points (their: group, index, x, y, z coordinates), each feature point first has to be checked if it is existant.
 * Feature point position on the face can be seen in  <a href="../mpeg-4_fba.pdf">mpeg-4_fba.pdf</a>
 */
void fdp_dump_to_console( FDP *fdp, unsigned char *file_name )
{
	int i, j, k;

	printf( "# FDP file.\n" );
	printf( "1.0 %s 0 0 0 0 0 0\n", file_name );

	// go through all groups of feature points
	for( i = FDP::FP_START_GROUP_INDEX; i < FDP::FP_END_GROUP_INDEX; i++ )
	{
		// go through all indexes in current feature point group
		for( j = 1; j <= fdp->groupSize(i); j++ )
		{
			// feature point is identified with group number, and index number
			const FeaturePoint &fp =  fdp->getFP( i, j );

			// if feature point is defined, then process it
			if( fp.defined )
			{
				// FP number, first is FP group, than FP index
				printf("    %d.%d ", i, j );
				for( k = 0; k < 3; k++ )
				{
					// feature point coordinates, x, y, z, in range 0..1.0
					printf( "%f ", (float) fp.pos[k] );
				}
				printf( "\n" );
			}
		}
	}
}

static size_t write_face_list(char *data, size_t size, size_t nmemb, void *stream)
{
	size_t realsize = size * nmemb;
	cout << "Reading data from server, stream points to:"<< stream << endl;
	cout << "Data is: " << data << endl;
	struct MemoryStruct *mem = new MemoryStruct();
	
	mem->memory = (unsigned char*)malloc(realsize);
	memmove(mem->memory, data, realsize);
	mem->size = realsize;
	
	*((MemoryStruct**)stream) = mem;
	return realsize;
}

static size_t write_cb(char *data, size_t size, size_t nmemb, void *stream){
	cout << "Size of the image: " << size*nmemb << endl;
	return size * nmemb;
}

vector<string> parse_mem_struct(MemoryStruct* mem){
	cout<<"parsing mem, size: " << mem->size << " ,data: " << mem->memory <<endl;
	vector<string> out;
	string temp;

	for(int i = 0; i < mem->size; ++i ){
		if(mem->memory[i] != '\n'){
			temp += mem->memory[i];
		} else {
			out.push_back(temp);
			temp.clear();
		}
	}

	return out;
}

vector<string> get_face_list_from_server(const char* server){
	CURL *curl;
	CURLcode res;
	vector<string> faces;
	MemoryStruct* mem = new MemoryStruct();

	// get a curl handle
   	curl = curl_easy_init();
	
   	if( !curl )		
	{
		printf( "Can't initialize cURL\n" );
		exit( EXIT_FAILURE );
	}
	
   	curl_easy_setopt( curl, CURLOPT_VERBOSE, 1L );
   	curl_easy_setopt( curl, CURLOPT_URL, server);
	curl_easy_setopt( curl, CURLOPT_HTTPGET, 1L );
   	curl_easy_setopt( curl, CURLOPT_REQUEST_TARGET, "/get_face_list");
	curl_easy_setopt( curl, CURLOPT_WRITEFUNCTION, write_face_list );

    curl_easy_setopt( curl, CURLOPT_WRITEDATA, &mem);
    
	res = curl_easy_perform( curl );

	// Check for errors
	if(res != CURLE_OK)
		fprintf( stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror( res ) );

	faces = parse_mem_struct(mem);

	// curl cleanup
	curl_easy_cleanup(curl);
	curl_global_cleanup();

	return faces;
}

static void add_transfer(CURLM *cm, const char* url)
{
	CURL *eh = curl_easy_init();
	curl_easy_setopt(eh, CURLOPT_WRITEFUNCTION, write_cb);
	curl_easy_setopt(eh, CURLOPT_URL, url);
	curl_easy_setopt(eh, CURLOPT_PRIVATE, url);
	curl_multi_add_handle(cm, eh);
}

int get_faces_from_server(const char* server){
	CURLM *curl;
	CURLMsg *msg;
	unsigned int transfers = 0;
	int msgs_left = -1;
	int still_alive = 1;
	CURLcode res;

	vector<string> faces = get_face_list_from_server(server);
	string url, filename;
	
	cout << "First face ID: " << faces[0] << endl;
 
	curl = curl_easy_init();
	
   	if( !curl )		
	{
		printf( "Can't initialize cURL\n" );
		exit( EXIT_FAILURE );
	}
	
   	curl_easy_setopt( curl, CURLOPT_VERBOSE, 1L );
   	curl_easy_setopt( curl, CURLOPT_URL, server);
	curl_easy_setopt( curl, CURLOPT_HTTPGET, 1L );
	
	for(transfers = 0; transfers < faces.size(); transfers++){
		url += "/face/";
		url += faces[transfers];
		
		filename += "faces/";
		filename += faces[transfers];
		filename += ".jpg";
		cout << "The file path is: " << filename << endl;
		FILE* out = fopen(filename.c_str(), "wb+");
		curl_easy_setopt( curl, CURLOPT_WRITEDATA, out);
		curl_easy_setopt(curl, CURLOPT_REQUEST_TARGET, url.c_str() );
		
		res = curl_easy_perform( curl );
		
		fclose(out);
		
		filename.clear();
		url.clear();
	}
	
	curl_easy_cleanup(curl);
	curl_global_cleanup();
	
	return res;
}

int connect_to_server(const char* server, int tries){
	int status = 1;
	
	for (int i = 0; (i < tries) && (0 != status); ++i){
		status = get_faces_from_server(server);
		if(0 != status){
			cout << "Failed to connect to sever, sleeping for 5 seconds" << endl;
			sleep(5);
		}else {
			cout << "Got faces" << endl;
		}
	}
	
	return status;
}

void drawResults(VsImage* image)
{
	cv::Mat mat(image->height, image->width, CV_8UC3, (void*)image->imageData);
	cv::imshow( "VisageTracker", mat );
	cv::waitKey( 10 );
}

void addFace(const char* name, VsImage* image, VisageFaceRecognition* fVFR, VisageFeaturesDetector *vsDetector ){
	FaceData vsDetFaceData;

	// Do detection in image, return value is number of detected faces; maximum allowed number of faces to detect is VISAGE_MAX_FACES_TO_HANDLE
	vsDetector->detectFacialFeatures( image, &vsDetFaceData, VISAGE_MAX_FACES_TO_HANDLE, 0.1f );
	//add it to gallery
	int stat = fVFR->addDescriptor(image, &vsDetFaceData, name);
}

vector<string> getFacesInDir(const char* dir_name){
	DIR* dir;
	struct dirent *ent;
	vector<string> files;
	
	if ((dir = opendir(dir_name)) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			files.push_back(ent->d_name);
		}
		closedir(dir);
} 	else {
		/* could not open directory */
		perror("");
	}
	
	return files;
}

CURLcode sendResults(CURL* curl, const char* name, float similarities){
	string data = "name=";
	data += name;
	data += "&similarity=";
	data += to_string(similarities);
	
	cout << data <<endl;
	
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data.c_str());
	
	return curl_easy_perform(curl);
	
}

#define CALC_32(field,index)		field[index] + (field[index+1]<<8) + (field[index+2]<<16) + (field[index+3]<<24)
#define CALC_16(field,index)		field[index] + (field[index+1]<<8)
#define VS_BMPSTRUCTSIZE      54

int main ( int argc, char *argv[] ){
	// Visage DETECTOR variables
	VisageFeaturesDetector *vsDetector = NULL;
	FaceData  *vsDetFaceData = NULL;
	VsImage   *vsDetImage = NULL;
	int vsDetNumFaces = 0;
	unsigned int vsDetInit;

	// Visage Recogniser variables
	VisageFaceRecognition* fVFR = NULL;
	char data_to_send[1024];

	int options;
	bool imageSpecified = false;
	bool serverSpecified = false;
	const char* serverURL = "198.162.0.50:8088";

    // initialize licensing, sw will not automatically search for license
	initializeLicenseManager(licenseKey.c_str());

	//Request faces from server
	int server_stat = connect_to_server(SERVER, 3);
	if( 0!= server_stat){
		return server_stat;
	}
  
  	// Create instance of VisageFeaturesDetector object
	vsDetector = new VisageFeaturesDetector();
	
	fVFR = new VisageFaceRecognition(DATAPATH2);

	raspicam::RaspiCam Camera; //Camera object
	
	raspicam::RASPICAM_FORMAT format = raspicam::RASPICAM_FORMAT_RGB;
	
	int visageFormat = VISAGE_FRAMEGRABBER_FMT_RGB;
	
	int nChannels = 3;
	
	unsigned int width = 640;
	unsigned int height = 480;

	Camera.setFormat(format);
	Camera.setCaptureSize(width, height);

	if ( !Camera.open()) {cerr<<"Error opening camera"<<endl;return -1;}

    //wait a while until camera stabilizes
    cout<<"Sleeping for 3 secs"<<endl;
    sleep(3);
    //allocate memory

	printf("%d\n", Camera.getImageTypeSize ( raspicam::RASPICAM_FORMAT_RGB));

	cv::namedWindow( "VisageTracker", 1 );
	
	// Initialize the detector with .bdf files located in "bdtsdata" directory
	// which is subdirectory of current directory (place from where executable was started )
	if (!( vsDetInit = vsDetector->Initialize( DATAPATH )) )
	{
		printf("Could not load detector data files.\n");
		return 1;
	}
	
	vector<string> files = getFacesInDir("./faces");
	
	for(int i = 1; i < files.size() - 1; ++i){
		string file_path = "./faces/";
		file_path += files[i];
		
		IplImage* f = cvLoadImage(file_path.c_str());	
		VsImage* im = vsCreateImageHeader(vsSize(f->width, f->height), f->depth, f->nChannels);
		
		vsSetImageData(im, f->imageDataOrigin, f->widthStep);
		
		string filename = files[i].substr(0, files[i].find_last_of(".")); 
		
		addFace(filename.c_str(), im, fVFR, vsDetector);
		
		vsReleaseImageHeader(&im);
		cvReleaseImage(&f);
	}
	
	// Create instance of VisageFaceRecognistion
	int descriptor_size = fVFR->getDescriptorSize();

    cout << "Initiatized" << endl;

	cout << "Resolution " << Camera.getWidth() << "x" << Camera.getHeight() << " bufferSize " << Camera.getImageBufferSize() << endl;
	
    unsigned char *data = new unsigned char[ Camera.getImageBufferSize() ];
    
    Camera.grab();

	// Create FaceData object, it will hold results of detection
	vsDetFaceData = new FaceData[VISAGE_MAX_FACES_TO_HANDLE];
	
	// Set up CURL to post results
	
	CURL* post_curl;
	CURLcode post_res;
	
	post_curl = curl_easy_init();
	curl_easy_setopt(post_curl, CURLOPT_URL, SERVER);
	curl_easy_setopt(post_curl, CURLOPT_REQUEST_TARGET, "/face_result");

	while (1){
    	//capture
    	Camera.grab();
    	//extract the image in rgb format
    	Camera.retrieve ( data );//get camera image
    	
    	VsImage* tempImage = vsCreateImageHeader(vsSize(Camera.getWidth(), Camera.getHeight()), 8, nChannels);
		vsSetData(tempImage, (void*)data, tempImage->widthStep);
    	
		vsDetImage = vsCreateImage(vsSize(Camera.getWidth(), Camera.getHeight()), 8, nChannels);
		vsCvtColor(tempImage, vsDetImage, VS_RGB2BGR);
		if (tempImage) vsReleaseImageHeader(&tempImage);
	
		// Do detection in image, return value is number of detected faces; maximum allowed number of faces to detect is VISAGE_MAX_FACES_TO_HANDLE
		vsDetNumFaces = vsDetector->detectFacialFeatures( vsDetImage, vsDetFaceData, VISAGE_MAX_FACES_TO_HANDLE, 0.1f );
		
		for(int i = 0; i < vsDetNumFaces; i++){
			const char* names;
			float similarities = 0; 


			short* descriptor = new short[descriptor_size];
			int status = fVFR->extractDescriptor(&vsDetFaceData[i], vsDetImage, descriptor);
			
			fVFR->recognize(descriptor, 1, &names, &similarities);
			cout << names << " similarity % "<< similarities << endl;
			
			post_res = sendResults(post_curl, names, similarities);
		}
		
		//drawResults(vsDetImage);
	}
	
    curl_easy_cleanup(post_curl);

    return 0;
}

