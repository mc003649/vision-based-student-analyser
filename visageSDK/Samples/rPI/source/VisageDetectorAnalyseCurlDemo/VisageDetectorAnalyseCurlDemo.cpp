/*! \file VisageDetectorAnalyseCurlDemo.cpp
 *  \brief Doing analysis on image, with - data made with visage features detector, and sending analysis data with cURL.
 *
 *  Image, only bitmap file with 24 bits per pixel, is loaded with accompanied function, detectFacialFeatures (from visage features detector)
 *  call is executed to get data, and on that data analysis is performed.
 */
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <unistd.h> 
#include <curl/curl.h>
#include "visageVision.h"
#include "vs_core_core_c.h"
#include "LicenseString.h"

#define VISAGE_MAX_FACES_TO_HANDLE  20

using namespace std;
using namespace VisageSDK;

const char* helpMessage =
" ./visageDetectorAnalyseCurlDemo [options] \n"
" Options: \n"
" -i,     Specify image data path.\n"
" -s,     Specify server URL.\n\n";

/**
 * Visage analysis curenty estimates this seven emotions
 */
const char *visage_emotions[7] = {"Emotion", "Anger", "Disgust", "Fear", "Happiness", "Sadness", "Surprise"};

/**
 * Prints ALL defined (existing) feature points (their: group, index, x, y, z coordinates), each feature point first has to be checked if it is existant.
 * Feature point position on the face can be seen in  <a href="../mpeg-4_fba.pdf">mpeg-4_fba.pdf</a>
 */
void fdp_dump_to_console( FDP *fdp, unsigned char *file_name )
{
	int i, j, k;

	printf( "# FDP file.\n" );
	printf( "1.0 %s 0 0 0 0 0 0\n", file_name );

	// go through all groups of feature points
	for( i = FDP::FP_START_GROUP_INDEX; i < FDP::FP_END_GROUP_INDEX; i++ )
	{
		// go through all indexes in current feature point group
		for( j = 1; j <= fdp->groupSize(i); j++ )
		{
			// feature point is identified with group number, and index number
			const FeaturePoint &fp =  fdp->getFP( i, j );

			// if feature point is defined, then process it
			if( fp.defined )
			{
				// FP number, first is FP group, than FP index
				printf("    %d.%d ", i, j );
				for( k = 0; k < 3; k++ )
				{
					// feature point coordinates, x, y, z, in range 0..1.0
					printf( "%f ", (float) fp.pos[k] );
				}
				printf( "\n" );
			}
		}
	}
}


#define CALC_32(field,index)		field[index] + (field[index+1]<<8) + (field[index+2]<<16) + (field[index+3]<<24)
#define CALC_16(field,index)		field[index] + (field[index+1]<<8)
#define VS_BMPSTRUCTSIZE      54
VsImage* load_bmp24( unsigned char *name )
{
	FILE *fp;
	unsigned char bmp_fh[VS_BMPSTRUCTSIZE], *img_pixels;
	unsigned int file_size, pi_offset, bmih_size, img_wi, img_he, img_bpp, img_size, img_ncol;
	VsImage* image;

	fp = fopen( (const char *) name, "rb" );
	if ( fp == NULL )					{
		printf( "%s: file: %s can not be opened!\n", __FUNCTION__, name );
		return NULL;
	}

	fread( bmp_fh, 1, VS_BMPSTRUCTSIZE, fp );     // load bitmap file header
	if ( ( bmp_fh[0] != 'B' ) || ( bmp_fh[1] != 'M' ) )
	{
		printf("%s: file: %s is not a bitmap file!\n", __FUNCTION__, name );
		fclose( fp );    // no magic bytes, close file
		return NULL;		 // and fail
	}

	if ( ( bmih_size = CALC_32( bmp_fh, 14 ) ) != 40 )
	{
		printf("File: %s is not a supported bitmap file!\n", name );
		fclose( fp );
		return NULL;		// not a Windows BITMAPINFOHEADER, fail
	}

	file_size 	= CALC_32( bmp_fh, 2);		// file size embedded in bitmap header
	pi_offset 	= CALC_32( bmp_fh, 10);		// pixel offset from start of file, embedded in bitmap header

	img_wi = CALC_32( bmp_fh, 18 );     // width of image which will be loaded
	img_he = CALC_32( bmp_fh, 22 );     // height of image which will be loaded

	if ( CALC_16( bmp_fh, 26) != 1 )
	{
		fclose( fp );
		return NULL;		// number of planes not 1, fail
	}

	if ( ( img_bpp = CALC_16( bmp_fh, 28 ) ) != 24 )
	{
		fclose( fp );
		return NULL;		// not 24 bits per pixel, fail
	}

	if ( CALC_32( bmp_fh, 30 ) != 0 )		// compression we are only processing is BI_RGB, which is defined as 0
	{
		fclose( fp );
		return NULL;		// not BI_RGB compression, fail
	}

	img_size = CALC_32( bmp_fh, 34 );
	if ( ( img_ncol = CALC_32( bmp_fh, 46 ) ) != 0 )
	{
		printf( "Defined number of colours, ignoring it!\n" );
	}

	unsigned int skip = pi_offset - VS_BMPSTRUCTSIZE;		// calculate gap
	unsigned char c;
	while ( skip )							// skip gap
	{
		fread( &c, 1, 1, fp );
		skip--;
	}
	int c_he = (int) img_he;
	if ( ( (int) img_he ) < 0 )		// if height is negative, image is top-down orientation
		c_he = -1 * c_he;

	int widthStep = 4 * ( ( ( ( img_wi * img_bpp ) / 8 ) + 3 ) / 4 );	//  size of each row is rounded up to a multiple of 4 bytes (a 32-bit DWORD) by padding

	int c_size = c_he * widthStep;
	if ( img_size != (unsigned int) c_size )
	{
		printf( "image size (%d) not equal calculated image size (%d)\n", img_size, c_size );
	}

	img_pixels = ( unsigned char * ) malloc( c_size );
	fread( img_pixels, 1, c_size, fp );			// load pixel data
	fclose( fp );

	// now we have all data, so we can construct IplImage

	// do not create image data, we will supply allready allocated buffer as image data
	image = vsCreateImageHeader( vsSize( img_wi, c_he ), VS_DEPTH_8U, img_bpp/8 );
	vsSetImageData( image, img_pixels, widthStep );

	image->origin = ( ( (int) img_he ) < 0 ) ?  0  :  1 ;  // if height < 0, image is top-down, in open cv that is indicated with 0, otherwise image is bottom-up, in open cv that is indicated with 1

	vsFlip( image, image, 0 );			// strange, without this cvShowImage, shows image OK, but tracker inverts it, now we have situation that cvShowImage shows vertically flipped, but in tracker it is with correct orientation
	return image;
}

/**
 * Prints initialisation errors for visage face analysis.
 */
void face_analyse_init_print_errors( int gVFAInit )
{
	if( ( gVFAInit & VFA_AGE ) != VFA_AGE )
	{
		printf( "Not loaded age data files!\n" );
	}

	if ( ( gVFAInit & VFA_EMOTION ) != VFA_EMOTION )
	{
		printf( "Not loaded emotion data files!\n" );
	}

	if ( ( gVFAInit & VFA_GENDER ) != VFA_GENDER )
	{
		printf( "Not loaded gender data files!\n" );
	}
}

/**
 * Sends extracted analytical data to http server using cURL library. 
 */
void send_data_to_sever(char* data, char* server)
{
	CURL *curl;
	CURLcode res;

	// get a curl handle
   	curl = curl_easy_init();
	
   	if( !curl )		
	{
		printf( "Can't initialize cURL\n" );
		exit( EXIT_FAILURE );
	}
	
	// ask libcurl to show us the verbose output
   	curl_easy_setopt( curl, CURLOPT_VERBOSE, 1L );

	// set destination for data,
   	curl_easy_setopt( curl, CURLOPT_URL, server);
	
	// size of the POST data
	curl_easy_setopt( curl, CURLOPT_POSTFIELDSIZE, strlen( data ) );

	// Now specify the POST data
	curl_easy_setopt( curl, CURLOPT_POSTFIELDS, data );

	// Perform the request, res will get the return code
	res = curl_easy_perform( curl );

	// Check for errors
	if(res != CURLE_OK)
		fprintf( stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror( res ) );
	
	// curl cleanup
	curl_easy_cleanup(curl);
	curl_global_cleanup();
}

namespace VisageSDK //all visage|SDK calls are in visageSDK namespace
{
    void initializeLicenseManager(const char *licenseKeyFileFolder);
}

int main( int argc, char* argv[] )  {

	unsigned  char *file_to_load = NULL;

	// Visage DETECTOR variables
	VisageFeaturesDetector *vsDetector = NULL;
	FaceData  *vsDetFaceData = NULL;
	VsImage   *vsDetImage = NULL;
	int vsDetNumFaces = 0;
	unsigned int vsDetInit;

	// Visage Analyser variables
	VisageFaceAnalyser* gVFA = NULL;
	int gVFAInit = 0;
	int gVFAGender = 0;
	float gVFAage = 0;
	int gVFAEmotionsEstimated = 0;
	float gVFAemo_estimates[8]={0.0f};
	
	char data_to_send[1024];

	int options;
	bool imageSpecified = false;
	bool serverSpecified = false;
	char* image;
	char* serverURL;

	while ((options = getopt(argc, argv, "i:s:h")) != -1) 
	{
        switch (options) 
		{
			case 'i':
					imageSpecified = true;
					image = optarg;
					break;
			case 's':
					serverSpecified = true;
					serverURL = optarg;
					break;
			case 'h':
					printf("%s", helpMessage);
					exit(0);
        }
    }
	
	if (!imageSpecified)
		file_to_load = (unsigned char *) "../Samples/rPI/data/images/test.bmp";
	else
		file_to_load = (unsigned char *) image;

	// we support loading of BMPs only, and they must be 24 bpp typ
	vsDetImage = load_bmp24( file_to_load );

	if ( vsDetImage == NULL )
	{
		printf( "Image %s not loaded\n", file_to_load );
		exit( EXIT_FAILURE );
	}

	// initialize licensing, sw will not automatically search for license
	initializeLicenseManager(licenseKey.c_str());

	// Create instance of VisageFeaturesDetector object
	vsDetector = new VisageFeaturesDetector();

	// Initialize the detector with .bdf files located in "bdtsdata" directory
	// which is subdirectory of current directory (place from where executable was started )
	if (!( vsDetInit = vsDetector->Initialize( "../Samples/data" )) )
	{
		printf("Could not load detector data files.\n");
		delete vsDetector;
		// image was created as header and data separately, so it must be released the same way
		free( vsDetImage->imageData );				// release pixel data
		vsReleaseImageHeader( &vsDetImage );		// release image header
	}

	// Create FaceData object, it will hold results of detection
	vsDetFaceData = new FaceData[VISAGE_MAX_FACES_TO_HANDLE];

	// Create instance of VisageFaceAnalyser
	gVFA = new VisageFaceAnalyser();

	// Initialize the analyser with .bdf files located in "bdtsdata" directory
	// which is subdirectory of current directory (place from where executable was started )
	const char *dataPath = "../Samples/data/bdtsdata/LBF/vfadata";
	gVFAInit = gVFA->init( dataPath );
	face_analyse_init_print_errors( gVFAInit );

	// Do detection in image, return value is number of detected faces; maximum allowed number of faces to detect is VISAGE_MAX_FACES_TO_HANDLE
	vsDetNumFaces = vsDetector->detectFacialFeatures( vsDetImage, vsDetFaceData, VISAGE_MAX_FACES_TO_HANDLE, 0.1f );

	printf( "Detected %d faces in image\n", vsDetNumFaces );

	// only if we have detected faces, we have valid data to process,
	// in that case we do analysis on each detected face, and print results of analyse
	for (int nF = 0; nF < vsDetNumFaces; nF++)
	{
		// 		char file_name[64];

		// we will print coordinates of nose tip, as indicator to which face data visage_emotions corresponds to
		const FeaturePoint &nose_tip = (&vsDetFaceData[nF])->featurePoints2D->getFP(9,3);
		if( nose_tip.defined )
		{
			printf( "Face %d, nose at (%f, %f)\n", nF + 1, (float) nose_tip.pos[0], (float) nose_tip.pos[1] );
		}
		// if gender initialisation was good, do gender estimation
		if ( gVFAInit & VFA_GENDER )
		{
			gVFAGender = gVFA->estimateGender( vsDetImage, &vsDetFaceData[nF] );
			if ( gVFAGender > -1 )
			{
				printf( "Gender=%s\n", gVFAGender==0 ? "FEMALE" : "MALE" );
				sprintf( data_to_send, "gender=%s", gVFAGender==0 ? "FEMALE" : "MALE" );
			}
			else
			{
				printf( "Gender: NOT ESTIMATED\n" );
				sprintf( data_to_send, "gender=%s", "NOT ESTIMATED" );
			}
		}

		// if age initialisation was good, do age estimation
		if ( gVFAInit & VFA_AGE )
		{
			gVFAage = gVFA->estimateAge( vsDetImage, &vsDetFaceData[nF] );
			printf( "Age %2.1f\n", gVFAage );
			sprintf( &data_to_send[strlen(data_to_send)], "&Age=%2.1f", gVFAage );
		}

		// if emotion initialisation was good, do emotion estimation
		if ( gVFAInit & VFA_EMOTION )
		{
			gVFAEmotionsEstimated = gVFA->estimateEmotion( vsDetImage, &vsDetFaceData[nF], gVFAemo_estimates );
			for(int j = 1; j < 7; j++)
			{
				printf("%s  %2.1f\n", (char *) visage_emotions[j], gVFAemo_estimates[j-1] * 100 );
				sprintf( &data_to_send[strlen(data_to_send)], "&%s=%2.1f", (char *) visage_emotions[j], gVFAemo_estimates[j-1] * 100 );
			}
		}
		
		// send data to sever only if IP address is passed as a argument to main()
		if(serverSpecified)
		{
			send_data_to_sever(data_to_send, serverURL);
		}
	}

	// clean up
	delete[] vsDetFaceData;
	delete vsDetector;
	delete gVFA;

	// image was created as header and data separately, so it must be released the same way
	free( vsDetImage->imageData );				// release pixel data
	vsReleaseImageHeader( &vsDetImage );		// release image header

   return EXIT_SUCCESS;
}

