/*! \mainpage  visage|SDK VisageDetectorAnalyseCurlDemo example project
*
* \htmlonly
* <table border="0">
* <tr>
* <td width="32"><a href="../../../../../bin/visageDetectorAnalyseCurlDemo"><img src="../../../../../doc/images/run_sample.png" border=0 title="Run Sample (this may not work in all browsers, in such case please run visageDetectorAnalyseCurlDemo from the visageSDK\bin folder.)"></a></td>
* <td width="32"><a href="../../../../../bin"><img src="../../../../../doc/images/open_bin_folder.png" border=0 title="Open Executable Folder (visageSDK\bin)"></a></td>
* <td width="32"><a href="../../../data"><img src="../../../../../doc/images/open_data_folder.png" border=0 title="Open Data Folder"></a></td>
* <td width="32"><a href="../../../build"><img src="../../../../../doc/images/open_project_folder.png" border=0 title="Open Project Folder"></a></td>
* <td width="32"><a href="../../../source/VisageDetectorAnalyseCurlDemo"><img src="../../../../../doc/images/open_source_folder.png" border=0 title="Open Source Code Folder"></a></td>
* </tr>
* </table>
* \endhtmlonly
*
* <h2>Dependencies</h2>
*
* Prerequisites for building samples are:
* <ul>
* <li>To install cURL library</li>
* <ul>
* <li>  sudo apt-get install libcurl4-openssl-dev</li>
* </ul>
* <li>To install openBLAS library</li>
* <ul>
* <li>  sudo apt-get install libopenblas-dev</li>
* </ul>
* </ul>
* \endhtmlonly
*
*
* \htmlonly
* <h2>Running prebuilt samples</h2>
* \endhtmlonly
*
* The VisageDetectorAnalyseCurlDemo example project demonstrates the following capabilities of visage|SDK:
*
* - facial features detection in still images
* - estimating age
* - estimating gender
* - estimating emotions
*
* To use the application:
* run the './visageDetectorAnalyseCurlDemo -i <imagePath> -s <serverURL>' command from the terminal (both paramters are optional).\n\n
* This will perform facial features detection on a still image (<imagePath>). If server URL is provided (<serverURL>), retreived data will be sent to the http server using POST method.\n                 
* There is a basic example of how to run http server in folder visageSDK/Samples/rPI/server, in http_post.py script, and server URL can be modified there.\n   
* If no image is provided, detection will be performed on a default image.\n\n
* The sample is modified to run from the visageSDK/bin folder. It changes the current directory to ../Samples/data folder where Face Detector.cfg configuration file is located. 
* Images are provided relative to that folder. The image should represent a face, in frontal pose and with a neutral expression. Several test images are available in the folder <a href="../../../data">Samples/rPI/data</a>.
*
*
* \htmlonly
* <h2>Building the project</h2>
*
* <p>
* The samples source files are located in the <a href="../../../source/VisageDetectorAnalyseCurlDemo">Samples/rPI/source/VisageDetectorAnalyseCurlDemo</a> subfolder of the visage|SDK for rPI folder.
* They are built using the provided Makefile located in visageSDK/Samples/rPI/build folder.
* </p>
* \endhtmlonly
*/
