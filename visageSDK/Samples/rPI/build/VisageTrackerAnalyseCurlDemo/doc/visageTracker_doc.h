/*! \mainpage  visage|SDK VisageTrackerAnalyseCurlDemo example project
*
* \htmlonly
* <table border="0">
* <tr>
* <td width="32"><a href="../../../../../bin/visageTrackerAnalyseCurlDemo"><img src="../../../../../doc/images/run_sample.png" border=0 title="Run Sample (this may not work in all browsers, in such case please run visageTrackerAnalyseCurlDemo from the visageSDK\bin folder.)"></a></td>
* <td width="32"><a href="../../../../../bin"><img src="../../../../../doc/images/open_bin_folder.png" border=0 title="Open Executable Folder (visageSDK\bin)"></a></td>
* <td width="32"><a href="../../../data"><img src="../../../../../doc/images/open_data_folder.png" border=0 title="Open Data Folder"></a></td>
* <td width="32"><a href="../../../build"><img src="../../../../../doc/images/open_project_folder.png" border=0 title="Open Project Folder"></a></td>
* <td width="32"><a href="../../../source/VisageTrackerAnalyseCurlDemo"><img src="../../../../../doc/images/open_source_folder.png" border=0 title="Open Source Code Folder"></a></td>
* </tr>
* </table>
* \endhtmlonly
*
* <h2>Dependencies</h2>
*
* Prerequisites for building the sample are:
* <ul>
* <li>Installed openCV 2 development files</li>
* <ul>
* <li>	sudo apt-get install libopencv-dev</li>
* </ul>
* <li>Installed RaspiCam API</li>
* <ul>
* <li>	Refer to RaspiCam homepage for compiling instructions</li>
* </ul>
* <li>Installed cURL library</li>
* <ul>
* <li>  sudo apt-get install libcurl4-openssl-dev</li>
* </ul>
* <li>Installed openBLAS library</li>
* <ul>
* <li>  sudo apt-get install libopenblas-dev</li>
* </ul>
* </ul>
* \endhtmlonly
*
*
* \htmlonly
* <h2>Running prebuilt samples</h2>
* \endhtmlonly
*
* The VisageTrackerAnalyseCurlDemo example project demonstrates the following capabilities of visage|SDK:
*
* - facial features tracking from camera
* - facial features tracking in still images
* - estimating age
* - estimating gender
* - estimating emotions
*
* <h3>Usage instructions:\n\n</h3>
* The application has two usage modes: tracking from camera and tracking from image. The modes are mutually exclusive and are selected by providing a command-line argument to the executable.\n
* <h4>1)Tracking from camera:</h4>
*	The tracking from camera mode is activated by passing the '-c' parameter to the executable along with the name of the desired API for accessing the camera.\n There are two available API's for accessing the camera - OpenCV and RaspiCam. RaspiCam API allows access to Raspberry Pi internal camera, while OpenCV API also enables usage of an external USB camera.\n The desired API is passed as a mandatory command-line parameter when the sample is started, e.g.:\n
	<i>./visageTrackerAnalyseCurlDemo -c opencv</i>\n
	or\n
	<i>./visageTrackerAnalyseCurlDemo -c raspicam</i>\n
	Tracking results are displayed on the screen with the OpenCV library, regardless of API choice.\n
* <h4>2)Tracking from image:</h4>
	The image tracking mode is activated by default if no arguments are passed to the executable - in this case tracking is performed on a default image.\n The sample is modified to run from the
	visageSDK/bin folder. It changes the current directory to ../Samples/data folder where the Facial Features Tracker - Low.cfg configuration file is located. Images are provided relative to that
	folder. The image should represent a face, in frontal pose and with a neutral expression. Several test images are available in the folder <a href="../../../data">Samples/rPI/data</a>.\n
	The path to a desired image may be passed as follows:\n
	<i>./visageTrackerAnalyseCurlDemo -i <imagePath>.</i>\n\n
	In the image tracking mode, a server URL may optionally be provided as a second argument, e.g.:\n
	<i>./visageTrackerAnalyseCurlDemo -i <imagePath> -s <serverURL></i>\n
	In this case, retrieved data can be sent to a HTTP server using the POST method. A basic example of how to run HTTP server exists in http_post.py script that is located in the
	visageSDK/Samples/rPI/server folder. The server URL can be modified there.\n
*
*
* \htmlonly
* <h2>Building the project</h2>
*
* <p>
* The sample source files are located in the <a href="../../../source/VisageTrackerAnalyseCurlDemo">Samples/rPI/source/VisageTrackerAnalyseCurlDemo</a> subfolder of the visage|SDK for rPI folder.
* They are built using the provided Makefile located in the visageSDK/Samples/rPI/build folder.
* </p>
* \endhtmlonly
*/
