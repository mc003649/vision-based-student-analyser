import BaseHTTPServer, os, cgi, sys
class httpServHandler( \
      BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(self):
       # get the arguments passed with the POST request
       self.query_string = self.rfile.read(int(self.headers['Content-Length']))
       # parse the query string into a dictionary
       self.args = dict(cgi.parse_qsl(self.query_string))
       # HTTP response line is sent, followed by Server and Date headers
       self.send_response(200)
       self.send_header('Content-type', \
                        'text/html')
       self.end_headers()
       self.wfile.write( "<h2>Handling Post</h2><P>" )
       self.wfile.write( "<li>Location: <b>%s</b>"%(self.path))
       self.wfile.write( \
           "<li>Arguments:<b>%s</b><hr>"%(self.args))
       print( "%s"%(self.args) )
       f = open('data.txt', 'a+')
       f.write("%s"%(self.args))
       f.close()


PORT_NUMBER = 8000
SERVER_ADRESS = "192.168.1.22"

# Server will serve files from directory "http"
os.chdir("http")

print "serving at adress", SERVER_ADRESS, "port", PORT_NUMBER
print "serving from directory ", os.getcwd()

server = BaseHTTPServer.HTTPServer( \
           (SERVER_ADRESS, PORT_NUMBER), httpServHandler)

#start server
server.serve_forever()
