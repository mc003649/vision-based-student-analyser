namespace VisageSDK

{

    /*! \mainpage  API

     * Visage|SDK consists of static libraries libVisageVision, libVisageGaze and libVisageAnalyzer. Sample applications also use third party library OpenCV.

     *

     * The following section provides an overview of key functionalities, gives links to most important classes and relevant sample projects, and lists required libraries to link.

     * The detailed information is found in the documentation of each class, reached through links in this section or links on the side menu, or through class reference.

     *
     * visage|SDK provides three libraries:
     * - \if LINUX_DOXY <i>libVisageVision.a </i>  \else  <i>libVisageVision.so</i> \endif  - face detection, alignment and face tracking
	 *  - \if LINUX_DOXY <i>libOpenBlas.so </i>  \else  <i>libOpenblas.so</i> \endif (for more information on how to install OpenBLAS library, please take a look at Samples section)
     * - \if LINUX_DOXY <i>libVisageGaze.a </i>  \else  <i>libVisageGaze.so</i> \endif  - screen space gaze tracking, requires:
	 *  - \if LINUX_DOXY <i>libVisageVision.a</i> \else  <i>libVisageVision.so</i> \endif \n
     * - \if LINUX_DOXY <i>libVisageAnalyser.a </i>  \else  <i>libVisageAnalyzer.so</i> \endif  - face analysis and recognition, requires: 
     *  - \if LINUX_DOXY <i>libVisageVision.a </i>  \else  <i>libVisageVision.so</i> \endif \n

     *

     * \section visageVision-t Facial features tracking

     * Visage tracker tracks multiple faces and facial features in video sequences and outputs 3D head pose, facial expressions, gaze direction, facial feature points and textured 3D face model.

     * The tracker is fully configurable in terms of performance, quality, tracked features and facial actions, as well as other options, allowing in effect a variety of customized trackers suited to different applications. Several common configurations are delivered. Details about configuring the tracker can be found in the <a href="../VisageTracker Configuration Manual.pdf">VisageTracker Configuration Manual</a>.

     *

     * Face tracking is demonstrated in VisageTrackerDemo sample project.

     *

     * <b>Main classes</b>

     * - VisageTracker: Tracks multiple faces and facial features in video coming from file, camera or other source.

     * - FaceData: Used to return tracking results from the tracker.

     *

     *

     * \section visageVision-d Facial feature detection

     * The class VisageFeaturesDetector detects faces and facial features in input images.

     * The results are, for each detected face, the 3D head pose, the coordinates of facial feature points, e.g. chin tip, nose tip, lip corners etc. and 3D face model fitted to the face.

     * The results are returned in one or more FaceData objects, one for each detected face.

     *

     * The FaceDetectDemo sample project demonstrates facial features detection.

     *

     * <b>Main classes</b>

     * - VisageFeaturesDetector: Detects facial features in still images.

     * - FaceData: Used to return the resulting feature points.

     *

     *

     *\section visageVision-a Facial feature analysis

     * The class VisageFaceAnalyser contains face analysis algorithms capable of estimating gender and emotion from frontal facial images (yaw between -20 and 20 degrees).

     * For gender estimation it returns estimated gender and for emotions estimation it returns the probability of each of estimated facial emotions: anger, disgust, fear, happiness, sadness, surprise and neutral.

     *
     * <b>Main classes</b>

     * - VisageFaceAnalyser: Face analyser capable of estimating gender and emotion from frontal facial images (yaw between -20 and 20 degrees).

     *
      
      *\section visageVision-r Face recognition
     
     * The class VisageFaceRecognition contains a face recognition algorithm capable of measuring similarity between human faces and recognizing a person's identity from frontal facial image (yaw angle approximately from -20 to 20 degrees) by comparing it to faces previously stored in a gallery. 
     
     *
     
     * <b>Main classes</b>
     
     * - VisageFaceRecognition: Measures similarity between human faces and recognizes a person's identity from frontal facial images (yaw angle approximately from -20 to 20 degrees). 
     
     */

}