<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>
<tagfile>
  <compound kind="struct">
    <name>VisageSDK::FaceData</name>
    <filename>structVisageSDK_1_1FaceData.html</filename>
    <member kind="variable">
      <type>float</type>
      <name>trackingQuality</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a6aca0c311aeb0b3ee0eead93546235dd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>frameRate</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a7138a72d57adc5ddb75d757bf0653dc1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>long</type>
      <name>timeStamp</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>af7afec396f9581356cc0b8fea4a10b9b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>faceTranslation</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a854b784b183fa4021e713506deff9845</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>faceRotation</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>ab5fe0bbe5ca937e2f2070c9fe07f3b82</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>gazeDirection</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a756d9a030db04d3e8686d8edd9c385ef</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>gazeDirectionGlobal</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a163adbed757c776cf6ce7556ec226e58</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>eyeClosure</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a4c494a1b319a79eade8191cd7e9b7b4c</anchor>
      <arglist>[2]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>shapeUnitCount</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a0c65746338cc1236659c950014b02f9d</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>shapeUnits</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a7bc3d34519d420010cab7829c5d24b85</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>actionUnitCount</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a05248f4d533bb6e4977ecba4eeb866d0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>actionUnitsUsed</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a49a558930595aea1140415bc4acf3aba</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>actionUnits</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a7fe570893d3cfda329549c2432450e5f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>const char **</type>
      <name>actionUnitsNames</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a3b816d03c066223724aac966ec535deb</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>FDP *</type>
      <name>featurePoints3D</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a1fd85018c51af5c63ac2d60eb6c9d91a</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>FDP *</type>
      <name>featurePoints3DRelative</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>ad64b091d44f2358f2cd0e86d7b627a99</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>FDP *</type>
      <name>featurePoints2D</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a98c7a959cbde274d14a92ef191e7758c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>faceModelVertexCount</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>abc8b508113ddfce9ddc012420920ce40</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>faceModelVertices</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a4e23ed5a39d923bd503e7e2fc59b10f0</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>faceModelVerticesProjected</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a9e799cc446b4bc942fdbbdf12bfab7a6</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>faceModelTriangleCount</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a710b1440e4969cb5ad3be4cacb3c1f0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>faceModelTriangles</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a8ee50ac55f282af229942fe4077b6434</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>faceModelTextureCoords</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>aad53581671d541e68e7bf9e4619d3274</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>faceModelTextureCoordsStatic</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a66b71ae9b4b3748c700ef8f2c07e9b98</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>faceScale</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a49182e24cf549a1d6c7506b5316e60d5</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>cameraFocus</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>af3c8554e62965d5bac968c3e4aa5ec82</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>ScreenSpaceGazeData</type>
      <name>gazeData</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>afa666a789971fb67bd4e485c505ba61b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>gazeQuality</name>
      <anchorfile>structVisageSDK_1_1FaceData.html</anchorfile>
      <anchor>a6eb38e94404ad438f07c8ac36ca7d3b0</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::FDP</name>
    <filename>classVisageSDK_1_1FDP.html</filename>
    <member kind="function">
      <type></type>
      <name>FDP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>acbbc90871ef46e7f1929b1a5698aac33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FDP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a729b30519019ffc7ec1e3e107add0273</anchor>
      <arglist>(const FDP &amp;featurePoints)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>FDP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a1d75bb3de86a0c47b4da06b8cf7c3f7b</anchor>
      <arglist>(const char *fn)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>readFromFile</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a40e621eae2e12ad20544e28e45c0e402</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>createNewFile</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>ab3a48e821fcf6bc5f0c4a5a1cac7e654</anchor>
      <arglist>(const char *name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>saveToFile</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a9c0e7e25f393a9482d5e393a5d6fe6c2</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>saveToFile</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a8ae67143d3676c56d0bc9e8a05a2c6dd</anchor>
      <arglist>(const char *fileName)</arglist>
    </member>
    <member kind="function">
      <type>const FeaturePoint &amp;</type>
      <name>getFP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a1a5507ba8c4fe8f640e63714e93e1757</anchor>
      <arglist>(int group, int n) const </arglist>
    </member>
    <member kind="function">
      <type>const FeaturePoint &amp;</type>
      <name>getFP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a1b37c3c7b0aa21ce869f63b4f3e1d15b</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>acd6f3fc0f5752bfbe58575ed9e8746e4</anchor>
      <arglist>(int group, int n, const FeaturePoint &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFP</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a3d4f635b162820cac467852bd7c001cd</anchor>
      <arglist>(const char *name, const FeaturePoint &amp;f)</arglist>
    </member>
    <member kind="function">
      <type>const float *</type>
      <name>getFPPos</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a771f7b6d72f0fdef3531fa45092bf790</anchor>
      <arglist>(int group, int n) const </arglist>
    </member>
    <member kind="function">
      <type>const float *</type>
      <name>getFPPos</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a19adcc1acc0bcdf6526a027900a027a5</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getFPQuality</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>abc3cb1794cc02307a1fbed90a24ed092</anchor>
      <arglist>(int group, int n) const </arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getFPQuality</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>ab730cf96167f183ee775d8e1e8444ef3</anchor>
      <arglist>(const char *name) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPQuality</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a2f535d3f500f83a217b0227ec939b6a5</anchor>
      <arglist>(int group, int n, const float quality)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPQuality</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a995c72a5f77f7b9fcad1ec13d28186aa</anchor>
      <arglist>(const char *name, const float quality)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPPos</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>af5104bfb7b793a9f584ee06bd698a6c0</anchor>
      <arglist>(int group, int n, const float *pos)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPPos</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a224157c63573689aa13209f62ee13cc0</anchor>
      <arglist>(const char *name, const float *pos)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPPos</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a90a288134b10bd0a2a0de17b99997336</anchor>
      <arglist>(int group, int n, float x, float y, float z)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPPos</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a45f3965e87b0ee16d2c7f3c85edc45cb</anchor>
      <arglist>(const char *name, float x, float y, float z)</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getFPSurfVert</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a63674c1f3ac20879446f39db320de79d</anchor>
      <arglist>(int group, int n, std::string &amp;surf, int &amp;vert) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>getFPSurfVert</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>ac8c6192623372ea02e2f0d1637991819</anchor>
      <arglist>(const char *name, std::string &amp;surf, int &amp;vert) const </arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPSurfVert</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>aa9a44e5a435d6d5b4539d3d1d9708e62</anchor>
      <arglist>(int group, int n, const std::string &amp;surf, int vert)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setFPSurfVert</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a78d6297c00bb813411f47ccff1160a83</anchor>
      <arglist>(const char *name, const std::string &amp;surf, int vert)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a822c3475d4533100c322c792b8ad563b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>FPIsDefined</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a2a9b0403969e106c716ff9d103fb7972</anchor>
      <arglist>(int group, int n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>FPIsDefined</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a98983a792c8bef17a1146ec86221a0eb</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>FPIsDetected</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a3a4f8451b0716c2befe76b414e822d1d</anchor>
      <arglist>(int group, int n) const </arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>FPIsDetected</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a682e90fc94497cb0959ab9f74be66d8c</anchor>
      <arglist>(const std::string &amp;name) const </arglist>
    </member>
    <member kind="function" static="yes">
      <type>static void</type>
      <name>parseFPName</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a26cf82f4dd81d871eb85f8433c43903f</anchor>
      <arglist>(const std::string &amp;name, int &amp;group, int &amp;n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static std::string</type>
      <name>getFPName</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a9a2e137097e05d7ac60860a2e185c5af</anchor>
      <arglist>(int group, int n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>FPIsValid</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a1f742eb9a07b8f70c3fa2e54337ccb88</anchor>
      <arglist>(int group, int n)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static bool</type>
      <name>FPIsValid</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a58ef08fcabcfca7d50b57f35ec5e916e</anchor>
      <arglist>(const std::string &amp;name)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>groupSize</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>af522a5088cb36697f9856bb5b1cdbc55</anchor>
      <arglist>(int group)</arglist>
    </member>
    <member kind="function" static="yes">
      <type>static int</type>
      <name>getMirrorPointIndex</name>
      <anchorfile>classVisageSDK_1_1FDP.html</anchorfile>
      <anchor>a69e7847756ce13e3ab64fa8bfdc80abc</anchor>
      <arglist>(int group, int index)</arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>VisageSDK::FeaturePoint</name>
    <filename>structVisageSDK_1_1FeaturePoint.html</filename>
    <member kind="variable">
      <type>float</type>
      <name>pos</name>
      <anchorfile>structVisageSDK_1_1FeaturePoint.html</anchorfile>
      <anchor>a3b8e2c0bce305f9170f3f6d5bb597ea8</anchor>
      <arglist>[3]</arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>defined</name>
      <anchorfile>structVisageSDK_1_1FeaturePoint.html</anchorfile>
      <anchor>a1948b9ad64c7d20439d43e5f3e8446c3</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>detected</name>
      <anchorfile>structVisageSDK_1_1FeaturePoint.html</anchorfile>
      <anchor>a28254625758e730fb56a0cd607b0060b</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>quality</name>
      <anchorfile>structVisageSDK_1_1FeaturePoint.html</anchorfile>
      <anchor>a85abdcf8b9a7ad1aba0d05b25c0f56de</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>vert</name>
      <anchorfile>structVisageSDK_1_1FeaturePoint.html</anchorfile>
      <anchor>a698e7a8dd2f79613c005cc15a7fd31a4</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>std::string</type>
      <name>surf</name>
      <anchorfile>structVisageSDK_1_1FeaturePoint.html</anchorfile>
      <anchor>a7ab3060ca6fa5eaa5c2867deee865fe4</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>VisageSDK::QualityParams</name>
    <filename>structVisageSDK_1_1QualityParams.html</filename>
    <member kind="function">
      <type></type>
      <name>QualityParams</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>a2af3120ac73247404de17ea578b421d1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>CalibTrackQuality</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>ae03c1036774d57a72782480b05ef1fa7</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>EstimTrackQuality</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>a18fbc44e63c34973e772907397599cef</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>EstimCumulativeRotation</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>a95f2b15a50ea1822259591753e459def</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>CalibCumulativeRotation</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>a0c20f797149169d38bedc4308f12d956</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>CalibCumulativeTranslation</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>a1250b56a1d57684dcaee6759d71e7733</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>CalibFramesCount</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>aa83e6abfd08b8222dc800e57b95868d1</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>CalibOutliersCount</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>a4c828f456fe59728a215eed9899ddd9f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>CalibDefinedCount</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>ad7c3f8e385eb5e0b1b0685a1f9c268cd</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>EstimFramesCount</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>ac7e304be50e7ca89dcf7ade563a6408c</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>CalibFramesStatus</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>af4003557655be82e4974abe099809e40</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int *</type>
      <name>EstimFramesStatus</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>ac2731705506f5bce79e7a7d889dca711</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float *</type>
      <name>CalibDetectQuality</name>
      <anchorfile>structVisageSDK_1_1QualityParams.html</anchorfile>
      <anchor>afedc648ef8041289dd7e1477331c8c91</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="struct">
    <name>VisageSDK::ScreenSpaceGazeData</name>
    <filename>structVisageSDK_1_1ScreenSpaceGazeData.html</filename>
    <member kind="variable">
      <type>int</type>
      <name>index</name>
      <anchorfile>structVisageSDK_1_1ScreenSpaceGazeData.html</anchorfile>
      <anchor>aaf15f134675110a96dfcaa293c52f083</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>x</name>
      <anchorfile>structVisageSDK_1_1ScreenSpaceGazeData.html</anchorfile>
      <anchor>a3d839820952ea57e81e342c97d80374f</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>y</name>
      <anchorfile>structVisageSDK_1_1ScreenSpaceGazeData.html</anchorfile>
      <anchor>ab6d19e525f06f656ab7cb015cbe70db2</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>int</type>
      <name>inState</name>
      <anchorfile>structVisageSDK_1_1ScreenSpaceGazeData.html</anchorfile>
      <anchor>a6951d3dfd2f0d5d8394bfc5dfb924e65</anchor>
      <arglist></arglist>
    </member>
    <member kind="variable">
      <type>float</type>
      <name>quality</name>
      <anchorfile>structVisageSDK_1_1ScreenSpaceGazeData.html</anchorfile>
      <anchor>a5206542ec6af0f58cba9cee08b92097e</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::ScreenSpaceGazeRepository</name>
    <filename>classVisageSDK_1_1ScreenSpaceGazeRepository.html</filename>
    <member kind="function">
      <type>ScreenSpaceGazeData *</type>
      <name>Get</name>
      <anchorfile>classVisageSDK_1_1ScreenSpaceGazeRepository.html</anchorfile>
      <anchor>a6f2050104a77a36e286dd1dfbfe0d6d5</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GetCount</name>
      <anchorfile>classVisageSDK_1_1ScreenSpaceGazeRepository.html</anchorfile>
      <anchor>a39c352b803043c7951fb107ed9cf5bbc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GetFirst</name>
      <anchorfile>classVisageSDK_1_1ScreenSpaceGazeRepository.html</anchorfile>
      <anchor>ac6b921ddafc3fe56b0100fd9e9069876</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>GetLast</name>
      <anchorfile>classVisageSDK_1_1ScreenSpaceGazeRepository.html</anchorfile>
      <anchor>a3639c617da6480d3bc2ea914be683c33</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Add</name>
      <anchorfile>classVisageSDK_1_1ScreenSpaceGazeRepository.html</anchorfile>
      <anchor>a2afcda375df93f4af08a5521c49431ac</anchor>
      <arglist>(ScreenSpaceGazeData *data)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>Add</name>
      <anchorfile>classVisageSDK_1_1ScreenSpaceGazeRepository.html</anchorfile>
      <anchor>a4d058e8a2d4a88d7635947e26dd2c988</anchor>
      <arglist>(int index, float x, float y)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::VisageFaceAnalyser</name>
    <filename>classVisageSDK_1_1VisageFaceAnalyser.html</filename>
    <member kind="function">
      <type></type>
      <name>VisageFaceAnalyser</name>
      <anchorfile>classVisageSDK_1_1VisageFaceAnalyser.html</anchorfile>
      <anchor>a11b2bff1ba5a647728dd10644d0bacd8</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~VisageFaceAnalyser</name>
      <anchorfile>classVisageSDK_1_1VisageFaceAnalyser.html</anchorfile>
      <anchor>a91b541c798ae77998ca3e370913773d4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>init</name>
      <anchorfile>classVisageSDK_1_1VisageFaceAnalyser.html</anchorfile>
      <anchor>a69321d8630e1a96bce693f00713b85ec</anchor>
      <arglist>(const char *dataPath)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>estimateAge</name>
      <anchorfile>classVisageSDK_1_1VisageFaceAnalyser.html</anchorfile>
      <anchor>a183b6b80ff43f49bd91ca969c48723e1</anchor>
      <arglist>(VsImage *frame, FaceData *facedata)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>estimateGender</name>
      <anchorfile>classVisageSDK_1_1VisageFaceAnalyser.html</anchorfile>
      <anchor>a9fdec0a9b8206a2ad846e4f7a83f37db</anchor>
      <arglist>(VsImage *frame, FaceData *facedata)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>estimateEmotion</name>
      <anchorfile>classVisageSDK_1_1VisageFaceAnalyser.html</anchorfile>
      <anchor>a8f8afd09fd98c7f1e7e99d9b4bc958c6</anchor>
      <arglist>(VsImage *frame, FaceData *facedata, float *prob_estimates)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::VisageFaceRecognition</name>
    <filename>classVisageSDK_1_1VisageFaceRecognition.html</filename>
    <member kind="function">
      <type></type>
      <name>VisageFaceRecognition</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a12d3e5c94eae1f2ab4d41cba0702cac8</anchor>
      <arglist>(const char *dataPath)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~VisageFaceRecognition</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a5a12acee786f99fba06dcf0f31a2ef2f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDescriptorSize</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a7a8845a91fdb250e6507ec33065de595</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>extractDescriptor</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a07a12ea770a8e822018a2322b00ae701</anchor>
      <arglist>(FaceData *facedata, VsImage *image, short *descriptor)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>descriptorsSimilarity</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a86f5c16de8390bf806dfb53fee0e9809</anchor>
      <arglist>(short *first_descriptor, short *second_descriptor)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addDescriptor</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>ac91c11f870c9316735fe470501f9261a</anchor>
      <arglist>(VsImage *image, FaceData *facedata, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>addDescriptor</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>abbfc70ad38d7a2b9e88ef07239a3135c</anchor>
      <arglist>(short *descriptor, const char *name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDescriptorCount</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>af69486334c7de336fdd4fd404af1b217</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>getDescriptorName</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a8aeed5002516004a63ececb15daa6827</anchor>
      <arglist>(char *name, int index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>replaceDescriptorName</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a62fe3a006eb9ca249b4ded43ed7c6f4b</anchor>
      <arglist>(const char *name, int index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>removeDescriptor</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a5fdcc980bcd7bca9bd408c9e7f48aee8</anchor>
      <arglist>(int index)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>saveGallery</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a05d7772a80ba58a5b4f5cbe34a3ab90c</anchor>
      <arglist>(const char *file_name)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>loadGallery</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a142642429c526b4da49f3f02bc10079f</anchor>
      <arglist>(const char *file_name)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>resetGallery</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a3a14f64d0180c758cd33569496b0db1b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>recognize</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a2f2a4e5faab573ece77aeca34858ba2c</anchor>
      <arglist>(short *descriptor, int n, const char **names, float *similarities)</arglist>
    </member>
    <member kind="variable">
      <type>bool</type>
      <name>is_initialized</name>
      <anchorfile>classVisageSDK_1_1VisageFaceRecognition.html</anchorfile>
      <anchor>a7b75b17fce5cb5c89e4030eabab22afe</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::VisageFeaturesDetector</name>
    <filename>classVisageSDK_1_1VisageFeaturesDetector.html</filename>
    <member kind="function">
      <type></type>
      <name>VisageFeaturesDetector</name>
      <anchorfile>classVisageSDK_1_1VisageFeaturesDetector.html</anchorfile>
      <anchor>ac4a75ef862227ed7edd84c9a621dce9d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~VisageFeaturesDetector</name>
      <anchorfile>classVisageSDK_1_1VisageFeaturesDetector.html</anchorfile>
      <anchor>a2b5ccb544e23a6255c88476b18dd928b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>bool</type>
      <name>Initialize</name>
      <anchorfile>classVisageSDK_1_1VisageFeaturesDetector.html</anchorfile>
      <anchor>ad23a5d91f896b82fa7f88dc3b9532e1a</anchor>
      <arglist>(const char *path)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>detectFaces</name>
      <anchorfile>classVisageSDK_1_1VisageFeaturesDetector.html</anchorfile>
      <anchor>a2ad8770f7f369325acabd6ebc3ef1cea</anchor>
      <arglist>(VsImage *frame, VsRect *faces, int maxFaces=1, float minFaceScale=0.1f, float maxFaceScale=1.0f, bool useRefinementStep=true)</arglist>
    </member>
    <member kind="function">
      <type>int</type>
      <name>detectFacialFeatures</name>
      <anchorfile>classVisageSDK_1_1VisageFeaturesDetector.html</anchorfile>
      <anchor>aa8163511b6cb27676b7c0145ab24a3a0</anchor>
      <arglist>(VsImage *frame, FaceData *output, int maxFaces=1, float minFaceScale=0.1f, float maxFaceScale=1.0f, bool outputOnly2DFeatures=false)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::VisageGazeTracker</name>
    <filename>classVisageSDK_1_1VisageGazeTracker.html</filename>
    <base>VisageSDK::VisageTracker</base>
    <member kind="function">
      <type></type>
      <name>VisageGazeTracker</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>a65acc04648e929a84f108515f8f64592</anchor>
      <arglist>(const char *trackerConfigFile)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>~VisageGazeTracker</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>a3f0bb6cecdb6f25f8eaf36b2a3457155</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>InitOnlineGazeCalibration</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>a2a392bb203cb31f6cf130b8e1926d34e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>AddGazeCalibrationPoint</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>ae4182976e7108f27277449e9ad02166e</anchor>
      <arglist>(float x, float y)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>FinalizeOnlineGazeCalibration</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>a2106b8a796afb6e94d55150b34b08ebb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>InitOfflineGazeCalibration</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>a6e30d5f08a1248af1b65e03982d754de</anchor>
      <arglist>(ScreenSpaceGazeRepository *calibrator)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>GetGazeEstimations</name>
      <anchorfile>classVisageSDK_1_1VisageGazeTracker.html</anchorfile>
      <anchor>acae907e7aeefe7c32c3e14cfe1972131</anchor>
      <arglist>(ScreenSpaceGazeRepository *repository)</arglist>
    </member>
    <member kind="function">
      <type></type>
      <name>VisageTracker</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a31f7894030a00f8f9ef937e4ec3dffbb</anchor>
      <arglist>(const char *trackerConfigFile)</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual</type>
      <name>~VisageTracker</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a7f6f4e3a76ff950305d1b590a1defc97</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function" virtualness="virtual">
      <type>virtual int *</type>
      <name>track</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>ad58ecb19c8e4adb16ee19be4e0187610</anchor>
      <arglist>(int frameWidth, int frameHeight, const char *p_imageData, FaceData *facedata, int format=VISAGE_FRAMEGRABBER_FMT_RGB, int origin=VISAGE_FRAMEGRABBER_ORIGIN_TL, int widthStep=0, long timeStamp=-1, int maxFaces=1)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setTrackerConfiguration</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a0f496426241db2487f4c4679f12e2a6a</anchor>
      <arglist>(const char *trackerConfigFile, bool au_fitting_disabled=false, bool mesh_fitting_disabled=false)</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>setIPD</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a6faea2ca70b77d653996c7dee849a460</anchor>
      <arglist>(float value)</arglist>
    </member>
    <member kind="function">
      <type>float</type>
      <name>getIPD</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a764786519941336bef07c44f0c92c075</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>stop</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a1642123d5415153eca075e1de27fffcd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>void</type>
      <name>reset</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>a4e149b87d27824f2fdc1dcf083fa27e1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <type>const char *</type>
      <name>getSDKVersion</name>
      <anchorfile>classVisageSDK_1_1VisageTracker.html</anchorfile>
      <anchor>abc11f105be17329c0ef5b51dbc719f53</anchor>
      <arglist>()</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>VisageSDK::VisageTracker</name>
    <filename>classVisageSDK_1_1VisageTracker.html</filename>
  </compound>
  <compound kind="dir">
    <name>doc</name>
    <path>/tmp/foo.QLEcnfhjd/visageSDK/doc/</path>
    <filename>dir_e68e8157741866f444e17edd764ebbae.html</filename>
    <file>linux_header.h</file>
  </compound>
  <compound kind="dir">
    <name>include</name>
    <path>/tmp/foo.QLEcnfhjd/visageSDK/include/</path>
    <filename>dir_d44c64559bbebec7f509842c48db8b23.html</filename>
    <file>FaceData.h</file>
    <file>FDP.h</file>
    <file>TrackerGazeCalibrator.h</file>
    <file>VisageFaceAnalyser.h</file>
    <file>VisageFaceRecognition.h</file>
    <file>VisageFeaturesDetector.h</file>
    <file>VisageGazeTracker.h</file>
    <file>VisageTracker.h</file>
  </compound>
  <compound kind="page">
    <name>index</name>
    <title>API</title>
    <filename>index</filename>
    <docanchor file="index" title="Facial features tracking">visageVision-t</docanchor>
    <docanchor file="index" title="Facial feature detection">visageVision-d</docanchor>
    <docanchor file="index" title="Facial feature analysis">visageVision-a</docanchor>
    <docanchor file="index" title="Face recognition">visageVision-r</docanchor>
  </compound>
</tagfile>
