\chapter{Literature Review}
\label{ch:lit_rev} %Label of the chapter lit rev. The key ``ch:lit_rev'' can be used with command \ref{ch:lit_rev} to refer this Chapter.

\section{Critique of existing work}
\label{sec:lit_critique}

This chapter analyses related products and papers to see what has been done before, their flaws and how they can be improved in this project. The project can be split into  multiple parts; the camera setup, face recognition algorithm, the computer which will perform recognition, database, analytic functions to perform on the data, reporting of the results.  

\subsection{Setup}
\label{sec:lit_setup}
\paragraph{Camera setup}
A number of the papers are putting the camera at the front of the classroom but the example images show a relatively small room \cite{cvucams, ieee, ramaiah}. For those classrooms it seems to be a good approach but it probably won't scale well for larger classrooms. With this set up, they require high definition cameras, which may even need to rotate to capture the whole of the class \cite{cvucams}. In these papers, the example image shows a classroom which is much smaller than some of the lecture halls that are used, to solve this, a higher resolution camera is required to be able to capture enough detail. It seems this makes the set up a lot less practical as it's not scalable.

Some papers are using black and white images to increase the computation speed, namely  \cite{ieee,irjet}. If a Raspberry Pi is not fast enough, this is a technique that could help without decreasing accuracy significantly, this could be tested.

\paragraph{Algorithm}
\label{sec:lit_algorithm}
Many different algorithms were used in the papers. They need to be considered because while VisageSDK is able to detect multiple faces in one frame, other algorithms might be a better fit for the this project \cite{visagesdk}. It's possible that some algorithms would be good enough at recognising faces while also being faster, therefor more suited to run on a low power machine.

The following paper uses background subsection for detection and eigenfaces for recognition \cite{ieee}. Background subtraction seams like a relatively simple algorithm to implement but it would require that the scene the camera is looking at doesn't change too much, otherwise it will not work. Also it will detect the person's body which isn't ideal, another algorithm will be needed to detect people in case their overlap.

Next paper uses skin classification for detection and feature extraction for recognition \cite{irjet}. Unfortunately they didn't describe it or reference it. The best possible one says it works best with one face in frame, but it should be possible to use its output to find multiple faces \cite{skinclassification}.

Another paper uses max margin face detection algorithm and have trained a Tensor Flow's Inception-V3 model to recognise students \cite{cvucams}. This algorithm uses histogram or oriented objects with max margin object detection, which thy claim is able to detect faces in a variety of positions. This seems like a potential algorithm that could be used in this solution, as long as it can be implemented.

The last paper uses the Viola-Jones algorithm for detection and Euclidean distance and k-nearest algorithms for recognition \cite{ramaiah}. This detection algorithm probably won't be viable for the purposes as it needs a unobstructed, frontal view of the face, not tilted in any way \cite{Viola01robustreal-time}. There is a good chance such shots can be taken, especially with good camera placement, but there should be better algorithms with fewer restrictions.

\paragraph{Recognition rate}
Most of the papers reviewed publish their recognition rates and they are good or understandably low. They all set up their cameras over a classroom full of participants so the recognition rates for this project's solution should be just as good or better since the camera will see a smaller number of participants in any given frame and their faces will be closer to the camera \cite{ramaiah,ieee,cvucams}.

One paper claims an accuracy of 100\% to 43\% depending on the number of people and the set of training images \cite{ramaiah}. The lowest score came from a test where only one training image was given per person, and it had to recognise 2 people. The most impressive result is the one with 3 training images per person, their system had an accuracy of 100\% for a room full of 20 people. 

The rest of the papers split their results into detection and recognition rates. Another paper shows results between 100\% and 90\% for recognition rate and 99.74\% to 99.4\% for detection rate depending on the detection algorithm being used \cite{cvucams}. This was tested on a classroom of 20 students in various poses.

The last paper has a face recognition between 87\% to 10\% and detection rate of 45\% to 93\% depending on if a person is veiled or has a beard \cite{ieee}. It will be good to test how well the system will be able to handle normal face obstructions, like beards, hats, sunglasses, etc. Though the data should be used to compare camera positioning and lighting as this project isn't testing how well VisageSDK works. 

\paragraph{Machine}
Since is going to use the Raspberry Pi 4 to capture the video feed, recognise the face and send the results to the server, it needs to have sufficiently powerful hardware to do all of this. It has a 64 bit, quad core running at 1.5GHz, between 1GB and 8GB of RAM and Gigabit ethernet port \cite{raspi4spec}.

The papers are usually vague about the system requirements of their algorithms and the machine they were using. The CVUCAMS paper fortunately isn't one of them, it specifies they used a 64 bit operating system with a processor running at 2.5GHz, 8GB of RAM and a 16MP high definition camera \cite{cvucams}. This machine is more powerful than the Raspberry Pi 4, but it does have to run Tensor-Flow, Inception-V3 so it would be advantageous for them to use higher spec machines for the learning phase \cite{cvucams}.

The paper from IRJET uses a camera with a resolution of 512 by 512, secondary memory to store the images and database, with a computer that runs MATLAB and Windows XP \cite{irjet}. Assuming that the lowest system requirements are limited the by software, then any Intel or AMD x86 processor that supports SSE2 instruction set will work, with 1GM of disk space and 2GB of RAM \cite{matlabreq}. This is a machine with low specifications, so it's a good indicator that a Raspberry Pi can be used for processing. Raspberry Pi 4 has a x64 quad processor which is better than some of the processors that can support MATLAB and it has sufficient amount of RAM \cite{intelpm740}.

\paragraph{Database}
So far few papers talk about security and how they store data \cite{ieee,irjet,ramaiah}. 
One does mention that they get and store all of their data on Moodle \cite{ieeeM}. Moodle stores hundreds of tables in their database but the security of them is left up to the user to an extent. The security recommendations provided are to have strong password policies, keep all software updated, back-up date, secure all pages, only give teacher accounts to trusted users and to use different passwords for different systems \cite{moodlesecurity}. These all look like good security recommendations that should be included in the design.

When considering how to encrypt the database and where to store the keys, papers which specifically discussed these details need to be considered. An encrypted database only protects data-at-rest, data-in-motion will be protected through either HTTPS or SSL and data-in-use is protected by hardware \cite{dbeoverview}. For the purposes of this project, only the column which stores the participants names and faces needs to be encrypted and since the mini PC should be low power, it must be quick to read. Following recommendations, only the fields that need to be encrypted, will be encrypted \cite{dbeoverview,dbeperformance}. This provides moderate speeds, but relatively low protection against information leakage and unauthorised modifications, though this is acceptable because as long as the private keys aren't compromised, participant's data can't be read \cite{dbeoverview}. 

To keep the cost down and portability of the system up, the server and database are on the same machine, but storing the private key on the same machine as the encrypted database seems rather insecure. If someone manages to get a hold of the machine, they will have both the data and the key.

\subsection{Analytics}
\label{sec:lit_analytics}
\paragraph{Graphs}
Not all of the sources talk about the reporting side, but the ones that do have different views for students, teachers and administrators \cite{creatrix,ieeeM}. 
It seems like the commercial product has more types of graphs and analytic functions than the research papers \cite{creatrix, ieeeM}.

The plugin described in the Moodle attendance plugin paper tracks the number of login to the application of ELearning Moodle, and the time spent in using E-Learning applications for each user. From that data, a number of graphs are generated like, a graph comparing how long each student spent logged in and on the course, and a graph of how many times the whole class logged in week by week. While they show multiple ways to process the data, only two types of graphs were shown \cite{ieeeM}.

\begin{figure}[H]
    \centering
    \includegraphics[scale=.55]{figures/Research/Student_graph(PENS).png}
    \caption{Example student graph}
    \label{fig:lit_student_graph}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[scale=.5]{figures/Research/Weekly_graph(PENS).png}
    \caption{Example weekly graph}
    \label{fig:lit_weekly_graph}
\end{figure}

The system will need similar graphs to show how students' attendance compares to each other and to see how the attendance of the whole class changes over time. Box plots can also be used to see just how the weekly attendance is distributed.

\paragraph{Reporting}
There will probably be too much data for attendance to manually look through the raw numbers, so the ability to configure and generate a report. For some use cases, just selecting data to be put on a graph will be enough, but others will need some statistical functions to process the data with. Standard statistical functions are necessary like average, mode, standard deviation, etc\cite{ieeeM}. The report should be able to show individual student's attendance, weekly attendance graphs, average time of arrival, on time verses unexplained absence verses explained absence, etc.

It would be interesting to add functions for multivariate data analysis to see if anything interesting can be found. Principal component analysis and cluster analysis can help show groups of students with similar attendance behaviour. Scatter plots and dendrograms must implemented to display this data \cite{attendancestats}.

Statistics on how the system performs are necessary, both for troubleshooting any problems and analysing the best way to set up the cameras. For that a separate report should be able to show the highest confidence a camera had, number of recognitions, time to recognise, malfunction notification(no camera, no network, security warning, needs update, etc.

\paragraph{User interface}
The webpages of the server need to be designed so that the users would be able to use all of the functions of the system. A concept which is used on webpage design are UI kits. The webpage is made out of smaller UI elements which keeps the website looking consistent and allows the users to re-use anything they have learnt about navigating a website \cite{effective_ui}. This means that common functionality should be used and any custom behaviour should be reused and kept to a minimum. 

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{figures/Research/set-attendance-flow.png}
    \caption{An example attendance sheet from the Creatix Campus website}
    \label{fig:creatrix_attendance}
\end{figure}

The repeated use of clear and concise use of UI elements can be seen in the figure \ref{fig:creatrix_attendance}. The names of the students is clearly displayed, each possibility "Present", "Absent", "Late" and "Leave" unambiguously explain what they mean through their iconography.

The layout of the webpage is also important. UI elements need to be arranged so that those which server a similar function, are close together but not too close that the page appears cluttered \cite{layout_ui}. White space should be used to separate out the elements and create a visual hierarchy.

\begin{figure}[H]
    \centering
    \includegraphics[scale=0.4]{figures/Research/display_reports.png}
    \caption{An example attendance graph from the Creatix Campus website}
    \label{fig:creatrix_graph}
\end{figure}

Creatix shows the use of whitespace to focus on the complex graph which helps the user understand it as can be seen in figure \ref{fig:creatrix_graph}. The graph takes up most of the screen, while leaving some whitespace to the left. It's at the top of the page so it's likely going to be the first thing the user sees. If the user isn't sure what the colours mean, the legend is close and very easy to find as it's the only other element on the page.

\paragraph{Notifications}
Sending notifications automatically is a feature of similar products, therefor the system should have the functionality to create triggers to automatically send notification \cite{creatrix}. Creatrix sends notifications from a report, so the trigger must be set up, this means that it can monitor processed data in real time. The system will have to either re-process all the data as more comes in every few seconds, or create the analysing functions so that they can handle streamed data. It might not be possible to implement some of the functions of the system to aggregate their results, so they will not update automatically.

\section{Summary}
\label{sec:lit_summery}

\begin{table}[H]
    \centering
    \caption{Summary for camera placement}
    \label{tab:lit_cam_summary}
    \begin{tabular}{lll}
        \hline
        Camera setup    & Pros                      & Cons                                  \\
        \hline
        \hline
        Front of class  & One camera, simple        & Needs to be high definition           \\
                        & No time restriction       & Might need to rotate                  \\
                        & Low hardware requirements & Low resolution for all students       \\
                        &                           & Performance based on classroom size   \\
        \hline
        Entrance        & Scalable                  & Must recognise quickly                \\
                        & Stationary                & Moderate hardware requirements        \\
                        & Higher resolution capture & Complexity from multiple cameras      \\
                        & Flexible number of cameras&                                       \\
                        & Not tested in other papers&                                       \\
        \hline
    \end{tabular}
\end{table}

From the table \ref{tab:lit_cam_summary}, it seems that it would be best to design the solution to have the cameras be placed at the entrance, due to the fact that the papers which were reviewed didn't do that and also because it aligns with the aim of creating a scalable solution as stated in section \ref{sec:intro_aims_obj}.

\begin{table}[H]
    \centering
    \caption{Summary for algorithm}
    \label{tab:lit_alg_summary}
    \begin{tabular}{lll}
        \hline
        Algorithm           & Pros  & Cons \\
        \hline
        \hline
        VisageSDK           & No need to train                  & Algorithm is unknown      \\
                            & Developed commercially            & No library experience     \\
                            & Has documentation                 &                           \\
        \hline
        Machine learning    & No domain knowledge to implement  & Needs multiple samples    \\
                            & Well known and tested             & Training takes resources  \\
        \hline
    \end{tabular}
\end{table}


In the table \ref{tab:lit_alg_summary}, all of the algorithms which were looked at in section \ref{sec:lit_algorithm} are under the "Machine Learning" row because they all use machine learning and feature mostly the same advantages and disadvantages. From this table VisageSDK seems to be a better option as it doesn't need to be trained with multiple samples and it is supported by a company.

The important aspects of the project have been researched. This project should be designed so that the cameras could be placed at the entrance of the classroom. The recognition rate should be better than from the papers which placed their cameras to over look the class, otherwise there are only disadvantages to placing them at an entrance. Sensitive data must be secure at all times, at rest and in motion. The system must be able to analyse the attendance data, data mining techniques should be applied to see how well they function. A user should be able to set up notifications so that users get an email if one of the user created notification triggers is fulfilled. 