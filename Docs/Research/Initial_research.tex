\documentclass[12pt, a4paper]{report}
\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage[english]{babel}
\usepackage{graphicx}
 
\usepackage[
backend=biber,
style=numeric-comp,
sorting=ynt
]{biblatex}
\addbibresource{references.bib}
\graphicspath{{./../images/}}

\title{Research}
\author{Nikita Rempel}

\begin{document}
\maketitle
\tableofcontents
\newpage

\chapter{Design}
\section{Introduction}
The purpose of this document is to examine previous work that is related to creating a vision base student analyser. This is to help my own project by finding out what has been done, what I can improve on and what I can learn.

I will outline my initial design and compare it to what others have already done to see how my design could be changed for the better.

\section{Initial design}
\subsection{Overview}
The hardware part of my system will consist of 2 different types of computers, Raspberry Pi's with cameras and a mini PC. The mini PC will connect to the cameras through a network, give them a set of faces to look for and listen for their reply. It will also act as a webserver the user can access through a web browser, they will be able to generate reports based on the attendance. The mini PC can be low end as it only needs to provide the website to a few users and cameras. Ideally it should have good network capability and a CPU with a few cores.

The Raspberry Pi's will need a camera each, at least one should be setup at each entrance. Each Pi will one only connect to one server, if there are multiple servers on a network, one server will be able to claim ownership so no other server will be able to connect to it until the first server stops. The Raspberry Pi's will run VisageSDK to recognise the faces. A camera will be connected to the Pi's so that they can capture a video stream, due to the planned setup, the normal Raspberry Pi camera should be sufficient to capture enough detail of a face to do recognition on. Visage claims that their face recognition algorithm is fast and lightweight as well as providing SDK support for Raspberry Pi\cite{visagesdk}, therefor I'm not too worried about the performance.
 
The webserver will provide analytic functions and collected data for the user to chose from in a report. These functions will generate graphs in the report. Alerts can be setup to notify when a condition is met, for example, if a student is late for too many lessons. The webserver will needs have users with different permissions for privacy and security. A student will not be able to look at the data of other students, teachers will not be able to look at data from other classes, administrators will not be able to modify/correct the register.

For the system to be useable, the website needs be able to add and remove students from the register, it needs to display in real time who has been marked in and who hasn't, the teacher must be able to set a student register a student as absent with and without a reason.

To be compliant with GDPR and ensure student's data is protected, the database which contains their faces and identification will be encrypted. All communication between camera and server will be encrypted. For the study, the participants names will not be stored on the database, but for a real world system, only the faces to recognise and pseudo-anonymised ID needs to be sent to the cameras. Once a camera recognises someone, their ID will be sent to the server as well as the confidence rating and time of recognition.

\section{Comparison}
\subsection{Setup}
\paragraph{Camera setup}
A number of the papers I have seen are putting the camera at the front of the classroom but the example images show a relatively small room\cite{cvucams, ieee, ramaiah}. For those classroom it seems to be a good approach but I'm sceptical that it scales well for larger classrooms. With this set up, they require high definition cameras, which may even need to rotate to capture the whole of the class\cite{cvucams}. In these papers, the example image shows a classroom which is much smaller than some of the lecture halls that could be used, to solve that, they would need a higher resolution camera to be able to capture enough detail. I think this makes the set up a lot less practice as it's not as scalable

Some papers are using black and white images to increase the computation speed\cite{ieee,irjet}. If I find that a Raspberry Pi is not fast enough, this is a technique that could help without decreasing accuracy too much, but I would have to test that.

\paragraph{Algorithm}
Many different algorithms were used in the papers. I'm interested mainly in the face detection algorithms as in the worst case, multiple faces will be in frame and VisageSDK only tracks one face at a time\cite{visagesdk} so I may need to be able to fins and switch to the other faces.

The first paper I want to look at uses background subsection for detection and eigen faces for recognition\cite{ieee}. Background subtraction seams like a relatively simple algorithm to implement but it would require that the scene the camera is looking at doesn't change too much, otherwise it will not help much. Also it will have to detect the person's body which isn't ideal, another algorithm will be needed to detect people in case their overlap.

Next paper uses skin classification for detection and feature extraction for recognition\cite{irjet}. Unfortunately they didn't describe it or reference it. The best one I found says it works best with one face in frame, but it should be possible to use its output to find multiple faces\cite{skinclassification}.

Another paper uses max margin face detection algorithm and have trained a Tensor Flow's Inception-V3 model to recognise faces\cite{cvucams}. This algorithm uses histogram or oriented objects with max margin object detection, which thy claim is able to detect faces in a variety of positions. This seems like a potential algorithm I could use, as long as I can implement it.

The last paper uses the Viola-Jones algorithm for detection and Euclidean distance and k-nearest algorithms for recognition\cite{ramaiah}. This detection algorithm probably won't be viable for my purposes as it needs a unobstructed, frontal view of the face, not tilted in any way\cite{Viola01robustreal-time}. There is a good chance such shots can be taken, especially with good camera placement, but there should be better algorithms for this purpose.

\paragraph{Recognition rate}
Most of the papers I have seen publish their recognition rates and they are good or understandably low. They all set up their cameras over a classroom full of participants\cite{ramaiah,ieee,cvucams} so I expect my own set up to perform just as well or better since I expect a smaller number of participants in any given frame.

One paper claims an accuracy of 100\% most of the time to 43\% depending on the number of people and the set of training images\cite{ramaiah}. The lowest score came from a test where only one training image was given per person, and it had to recognise 2 people. The most impressive result is the one with 3 training images per person, their system had an accuracy of 100\% for a room full of 20 people. 

The rest of the papers split their results into detection and recognition rates.Another paper shows results between 100\% and 90\% for recognition rate and 99.74\% to 99.4\% for detection rate\cite{cvucams} depending on the detection algorithm being used, either proposed method had the highest rate. This was tested on a classroom of 20 students in various poses.

The last paper I have results for, has a face recognition between 87\% to 10\% and detection rate of 45\% to 93\% depending on if it's veiled or with a beard\cite{ieee}. It will be good for me to test how well my system will be able to handle normal face obstructions, like beards, hats, sunglasses, etc. Though the data should be used to compare camera positioning and lighting as this project isn't testing how well VisageSDK works. 

\paragraph{Machine}
Since I'm planning on using the Raspberry Pi 4 to capture the video feed, recognise the face and send the results to the server, it needs to have sufficiently powerful hardware to do all of this. It has a 64 bit, quad core running at 1.5GHz, between 1GB and 8GB of RAM and Gigabit ethernet port\cite{raspi4spec}.

The details are usually vague about the system requirements if any are provided. The CVUCAMS paper\cite{cvucams} fortunately isn't one of them, it specifies a 64 bit operating system with a processor running at 2.5GHz, 8GB of RAM and a 16MP high definition camera. This machine is more powerful than the Raspberry Pi 4, but does have to run Tensor-Flow, Inception-V3\cite{cvucams} so it would be advantageous for them to use higher spec machines for the learning phase.

The paper from IRJET\cite{irjet} asks for a computer, a camera, resolution of 512 by 512 and secondary memory, with MATLAB and Windows XP. Assuming that the lowest system requirements are limited by software, then any Intel or AMD x86 processor that supports SSE2 instruction set will work, with 1GM of disk space and 2GB of RAM\cite{matlabreq}. This is a machine with low specification, so it's good news for the use of a RaspberryPi for processing as version 4 has an x64 quad processor which is better than some of the slowest processors that can support MATLAB\cite{intelpm740} and 2GB of RAM and higher.

\paragraph{Database}
So far few papers talk about security and how they store data\cite{ieee,irjet,ramaiah}. 
One does mention that they get and store all of their data on Moodle\cite{ieeeM}. Moodle stores hundreds of tables in their database but the security of them is left up to the user to an extent. The security recommendations provided are to have strong password policies, keep all software updated, back-up date, secure all pages, only give teacher accounts to trusted users and to use different passwords for different systems\cite{moodlesecurity}. These all look like good security recommendations that should be included in my design.

When considering how to encrypt the database and where to store the keys, I needed to look at papers which specifically discussed these details. An encrypted database only protects data-at-rest, data-in-motion will be protected through either HTTPS or SSL and data-in-use is protected by hardware\cite{dbeoverview}. For my purposes, only the column which stores the participants names and faces needs to be encrypted and since the mini PC should be low power, it should be quick to read. Following recommendations, only the fields that need to be encrypted, will be encrypted\cite{dbeoverview,dbeperformance}. This provides moderate speeds, but relatively low protection against information leakage and unauthorised modifications\cite{dbeoverview}, though this is acceptable because as long as the private keys aren't compromised, participant's data can't be read. 

To keep the cost down and portability of the system up, the server and database will be on the same machine, but storing the private key on the same machine as the encrypted database seems rather insecure. If someone manages to get a hold of the machine, they will have both the data and the key.

\subsection{Analytics}
\paragraph{Graphs}
Not all of the sources talk about the reporting side, but the ones that do have different views for students, teachers and administrators\cite{creatrix,ieeeM}. 
It seems like the commercial product has more types of graphs and analytic function\cite{creatrix} then the research paper\cite{ieeeM}.

The plugin described in the Moodle attendance plugin paper tracks the number of login to the application of ELearning Moodle, and the time spent in using E-Learning applications for each user. From that data, a number of graphs are generated like, a graph comparing how long each student spent logged in and on the course, and a graph of how many times the whole class logged in week by week. While they show multiple ways to process the data, only two types of graphs were shown\cite{ieeeM}.

\includegraphics[scale=.5]{Student_graph(PENS).png}

and

\includegraphics[scale=.4]{Weekly_graph(PENS).png}

I will need similar graphs to show how students' attendance compares to each other and to see how the attendance of the whole class changes over time. Box plots can also be sued to see just how the weekly attendance is distributed.

\paragraph{Reporting}
There will probably be too much data for attendance to manually look through the raw numbers, so the ability to configure and generate a report. For some use cases, just selecting data to be put on a graph will be enough, but others will need to some statistical functions to run the data through. Standard statistical functions are necessary like average, mode, standard deviation, etc\cite{ieeeM}. The report should be able to show individual student attendance, weekly attendance graphs, average time of arrival, on time vs unexplained absence vs explained absence, etc.

I think it would be interesting to add functions for multivariate data analysis to see if anything interesting can be found. PCA and clustering can help see groups of students with similar attendance behaviour. Scatter plots and dendrograms will need to be implemented to display this data.\cite{attendancestats}.

Statistics about how the system performs are also necessary, both for troubleshooting any problems and analysing the best way to set up the cameras. For that a separate report should be able to show the highest confidence a camera had, number of recognitions, time to recognise, malfunction notification(no camera, no network, security warning, needs update, etc

\paragraph{Notifications}
Sending notifications automatically is a feature of similar products\cite{creatrix}, therefor my system should have the functionality to create triggers to automatically send notification. Creatrix sends notifications from a report, so the trigger must be set up, this means that it can monitor processed data in real time. My system will have to either re-process all the data as more comes in every few seconds, or create the analysing functions so that they can handle streamed data. I'm not sure that some of the functions I want to implement can be written to aggregate their results, so I will chose on a case by case basis.
\newpage

\chapter{Requirements}
\section{Functional}
\subsection{Server}
\begin{itemize}
    \item Must be able to find Raspberry Pi cameras on the same network
    \item Must be able to communicate with the cameras to set the people to recognise and listen for when the cameras have seen them
    \item All connections to and from the server must be encrypted
    \item Sensitive data on the database must be encrypted
    \item A user must be able to connect to the server to view the register
    \item Only the logon screen will be accessible to unauthorised users
    \item An authorised user will be able to add, remove and modify the attendance sheet
    \item An authorised user will be able to set when attendance starts, ends and when class ends
    \item If a student is first recognised after attendance starts, they will be considered to be in
    \item If a student is first recognised after attendance ends, they will be considered late
    \item If a student is first recognised after class ends, they will be considered absent
    \item The register must update every time a student is recognised
    \item A user must be able to generate a report with the data collected and the given analysis functions
    \item The report can be set to update a set time after a student is recognised
    \item The report can be set to generate after the class ends or after the attendance ends
    \item The report must be able to display the data in a graph
    \item An authorised user must be able to set up notifications, which sends an email to a given account if a variable is below or above a threshold.
    \item An authorised user must be able to manually mark someone included
    \item When setting a password for a user, there must be sensible password restrictions
\end{itemize}
\subsection{Cameras}
\begin{itemize}
    \item Will integrate with VisageSDK
    \item Will parse a video stream to detect faces from a given list
    \item Must only connect to one server at a time
    \item Will try to recognise only when connected to a server
    \item When the camera recognises a student, it must send the result to the server as soon as possible
    \item All communication between itself and the server must be encrypted
    \item When the connection to the server is terminated, all personal data the camera might have must be removed
\end{itemize}
\section{Non-functional}
\begin{itemize}
    \item At least one camera must be able to recognise a student in the time it takes for them to walk through an entrance
    \item The server should be able to handle at least 5 camera connections and one browser connection
    \item The cameras must be portable
    \item The server software must be able to run on a Linux distribution
    \item The facial recognition software must be able to run on a Raspberry RaspberryPi
    \item A teacher user must not be able to see or modify attendance records of a different class
    \item Managers may look at the attendance records of any class but can not modify them
    \item Attendance records may not be changed after the class has ended
\end{itemize}
\newpage
\printbibliography[heading=bibintoc,title={References}]
\end{document}